#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T22:11:23
#
#-------------------------------------------------


QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MiniCAD
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    shapes.cpp \
    scene.cpp \
    view.cpp \
    interpreter.cpp \
    cuts.cpp \
    point.cpp \
    iobuttons.cpp \
    colorbuttons.cpp \
    shapebuttons.cpp \
    mymath.cpp \
    grip.cpp

HEADERS  += MainWindow.h \
    shapes.h \
    scene.h \
    view.h \
    interpreter.h \
    cuts.h \
    point.h \
    iobuttons.h \
    colorbuttons.h \
    shapebuttons.h \
    mymath.h \
    grip.h
