#ifndef POINT_H
#define POINT_H

#include <QPointF>

const double EPSILON = 0.0001;

class Point: public QPointF
{
public:
    bool operator>(const Point&) const;
    bool operator<(const Point&) const;
    bool operator==(const Point&) const;

    Point();
    Point(double, double);
    Point(QPointF);
    Point(const Point&);
    ~Point();
};

#endif // POINT_H
