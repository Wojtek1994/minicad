#include "point.h"

bool Point::operator>(const Point &p) const
// porownywanie jedynie wartosci x
{
    return x() > p.x();
}

bool Point::operator<(const Point &p) const
// porownywanie jedynie wartosci x
{
    return x() < p.x();
}

bool Point::operator==(const Point &p) const
{
    return pow(x() - p.x(), 2) + pow(y() - p.y(), 2) < EPSILON * EPSILON;
}

Point::Point(): QPointF()
{

}

Point::Point(double d1, double d2): QPointF(d1, d2)
{

}

Point::Point(QPointF p): QPointF(p)
{

}

Point::Point(const Point &p): QPointF(p.x(), p.y())
{

}

Point::~Point()
{

}

