#include "iobuttons.h"
#include "MainWindow.h"
#include <QFileDialog>
#include <QMessageBox>

void SaveButton::mousePressEvent(QMouseEvent *e)
{

}

void SaveButton::mouseReleaseEvent(QMouseEvent *e)
{
    QString filename = QFileDialog::getSaveFileName(this, "Saving..", "", "Files MiniCAD *.mcad");
    if (filename != "" && !filename.contains('.'))
    {
        filename.append(".mcad");
    }
    std::ofstream fs(filename.toStdString(), std::ios::binary);
    if (fs.good())
    {
        fs << dynamic_cast<MainWindow*>(parent())->getScene();
        fs.close();
        QMessageBox::information(this, "Information", "Image saved");
    }
    else
    {
        if (filename != "")
        {
            QMessageBox::warning(this, "Error", "Invalid path");
        }
    }
}

SaveButton::SaveButton(QWidget *parent): QPushButton(parent)
{
    setText("Save image");
    setVisible(true);
}

SaveButton::~SaveButton()
{

}


void OpenButton::mousePressEvent(QMouseEvent *e)
{

}

void OpenButton::mouseReleaseEvent(QMouseEvent *e)
{
    QString filename = QFileDialog::getOpenFileName(this, "Opening..", "", "Files MiniCAD (*.mcad)");
    std::ifstream fs(filename.toStdString(), std::ios::binary);
    if (fs.good())
    {
        dynamic_cast<MainWindow*>(parent())->getScene()->deleteAllItems();
        fs >> dynamic_cast<MainWindow*>(parent())->getScene();
        fs.close();
        QMessageBox::information(this, "Information", "Image opened");
    }
    else
    {
        if (filename != "")
        {
            QMessageBox::warning(this, "Error", "Invalid path");
        }
    }
}

OpenButton::OpenButton(QWidget *parent): QPushButton(parent)
{
    setText("Open image");
    setVisible(true);
}

OpenButton::~OpenButton()
{

}
