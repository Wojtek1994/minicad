#ifndef SHAPES_H
#define SHAPES_H

#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>
#include "grip.h"
#include "point.h"
#include <QColor>
#include <fstream>

#define PI 3.14159265358979323846


enum SHAPES;

const int OUTLINE_THICKNESS = 5;                // uzywane w metodzie trySelect, okresla margines bledu przy
const double OUTLINE_THICKNESS_PRECISE = 0.6;   // zaznaczaniu obiektow (w pikselach)

class Element
{
protected:
    Point centralPoint;
    double startRotation;

    bool resizable;
    bool rotatable;
    bool movable;
    bool checked;
    bool linesStyle;
    QColor penColor;
    int id;

public:
    Element();
    virtual ~Element();

    virtual void setElementBrush(const QColor&) = 0;
    virtual void setPenColor(const QColor&) = 0;
    virtual void setLinesStyle(bool) = 0;
    virtual void check(bool) = 0;
    virtual void norm(void) = 0;
    virtual SHAPES name(void) = 0;
    virtual void setResizable(bool) = 0;
    virtual void setMovable(bool) = 0;
    virtual void setRotatable(bool) = 0;
    virtual void updateGrips(void) = 0;
    virtual bool hasInHierarchy(QGraphicsItem*);

    bool isResizable(void);
    bool isRotatable(void);
    bool isMovable(void);
    bool isChecked(void);

    static Element *copyElement(Element*);
    void childId(const int &);

    friend std::ofstream &operator << (std::ofstream&, Element*);
    friend Element *readElement(std::ifstream&, int);

    virtual void setCentralPoint(const Point &) = 0;
    virtual void setStartRotation() = 0;

    virtual void move(const double &, const double &) = 0;
    virtual void rotate(const double &) = 0;

    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent*) = 0;

};



class Shape: public Element
{
protected:
    void setMovable(bool);
    void setRotatable(bool);

    virtual QRectF calcPosition(int) = 0;

public:
    virtual bool trySelect(const QRectF&, bool) = 0;
    virtual bool trySelect(const Point&, bool) = 0;
    static std::vector<Point> findCuts(Shape*, Shape*);

    virtual void setCentralPoint(const Point &);
    virtual void setStartRotation();
    double getTotalRotationAngle(void);
    virtual void check(bool);
    virtual void updateGrips(void);
    virtual void norm();
    void createGrips(int, int, Shape*);

    Shape();
    virtual ~Shape();
};



class Circle: public Shape, public QGraphicsEllipseItem
{
private:
    static const unsigned int CIRCLE_CENTRAL_GRIP_NUM = 1;
    static const unsigned int CIRCLE_BORDER_GRIP_NUM = 4;

    enum POSITION {TOP, DOWN, LEFT, RIGHT, CENTER};

    virtual QRectF calcPosition(int);

public:
    virtual void setElementBrush(const QColor&);
    virtual void setPenColor(const QColor&);
    virtual void setLinesStyle(bool);
    virtual void setResizable(bool);
    virtual SHAPES name(void);
    virtual bool trySelect(const Point&, bool);
    virtual bool trySelect(const QRectF&, bool);
    friend std::ofstream& operator << (std::ofstream&, Circle*);
    friend std::ifstream& operator >> (std::ifstream&, Circle*);

    Circle(const double &, const double &, const double &);
    Circle(void);
    Circle(Circle*);
    virtual ~Circle();

    void mouseMoveEvent(QGraphicsSceneMouseEvent*);

    virtual void move(const double &, const double &);
    virtual void rotate(const double &);

};

class Ellipse: public Shape, public QGraphicsEllipseItem
{
private:
    static const unsigned int ELLIPSE_CENTRAL_GRIP_NUM = 1;
    static const unsigned int ELLIPSE_BORDER_GRIP_NUM = 4;

    enum POSITION {TOP, DOWN, LEFT, RIGHT, CENTER};

    virtual QRectF calcPosition(int);


public:
    virtual void setElementBrush(const QColor&);
    virtual void setPenColor(const QColor&);
    virtual void setLinesStyle(bool);
    virtual void setResizable(bool);
    virtual SHAPES name(void);
    virtual bool trySelect(const Point&, bool);
    virtual bool trySelect(const QRectF&, bool);
    friend std::ofstream& operator << (std::ofstream&, Ellipse*);
    friend std::ifstream& operator >> (std::ifstream&, Ellipse*);

    Ellipse(const double &, const double &, const double &, const double &);
    Ellipse(void);
    Ellipse(Ellipse*);
    virtual ~Ellipse();

    void mouseMoveEvent(QGraphicsSceneMouseEvent*);

    virtual void move(const double &, const double &);
    virtual void rotate(const double &);

};


class Rectangle: public Shape, public QGraphicsRectItem
{
private:
    static const unsigned int RECTANGLE_CENTRAL_GRIP_NUM = 1;
    static const unsigned int RECTANGLE_BORDER_GRIP_NUM = 4;

    enum POSITION {TOPL, DOWNL, TOPR, DOWNR, CENTER};

    virtual QRectF calcPosition(int);
    QRectF calcResizing(Point, Point);

public:
    virtual void setElementBrush(const QColor&);
    virtual void setPenColor(const QColor&);
    virtual void setLinesStyle(bool);
    virtual void setResizable(bool);
    virtual SHAPES name(void);
    virtual bool trySelect(const Point&, bool);
    virtual bool trySelect(const QRectF&, bool);
    friend std::ofstream& operator << (std::ofstream&, Rectangle*);
    friend std::ifstream& operator >> (std::ifstream&, Rectangle*);


    Rectangle(const double &, const double &, const double &, const double &);
    Rectangle(void);
    Rectangle(Rectangle*);
    virtual ~Rectangle();

    void mouseMoveEvent(QGraphicsSceneMouseEvent*);

    virtual void move(const double &, const double &);
    virtual void rotate(const double &);

};


class Line: public Shape, public QGraphicsLineItem
{
private:
    static const unsigned int LINE_CENTRAL_GRIP_NUM = 1;
    static const unsigned int LINE_BORDER_GRIP_NUM = 2;

    enum POSITION {BEGIN, END, CENTER};

    virtual QRectF calcPosition(int);

public:
    virtual void setElementBrush(const QColor&);
    virtual void setPenColor(const QColor&);
    virtual void setLinesStyle(bool);
    virtual void setResizable(bool);
    virtual SHAPES name(void);
    virtual bool trySelect(const Point&, bool);
    virtual bool trySelect(const QRectF&, bool);
    friend std::ofstream& operator << (std::ofstream&, Line*);
    friend std::ifstream& operator >> (std::ifstream&, Line*);

    Line(const double &, const double &, const double &, const double &);
    Line(void);
    Line(Line*);
    virtual ~Line();

    void mouseMoveEvent(QGraphicsSceneMouseEvent*);

    virtual void move(const double &, const double &);
    virtual void rotate(const double &);

};


class Group: public Element, public QGraphicsItemGroup
{
private:
    Point groupPoint;

public:
    virtual void setElementBrush(const QColor&);
    virtual void setPenColor(const QColor&);
    virtual void setLinesStyle(bool);
    virtual void setMovable(bool);
    virtual void setRotatable(bool);
    virtual void setResizable(bool);
    virtual void check(bool);
    virtual void norm();
    virtual SHAPES name(void);
    virtual void setCentralPoint(const Point &);
    virtual void setStartRotation();
    virtual void updateGrips(void);
    friend std::ofstream& operator << (std::ofstream&, Group*);
    friend std::ifstream& operator >> (std::ifstream&, Group*);


    Group(Point);
    Group();
    Group(Group*);
    virtual ~Group();

    void mouseMoveEvent(QGraphicsSceneMouseEvent*);

    void addElement(Element*);
    Point getGroupPoint();

    virtual void move(const double &, const double &);
    virtual void rotate(const double &);
};

#endif
