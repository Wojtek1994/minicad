#ifndef COLORBUTTONS_H
#define COLORBUTTONS_H

#include <QPushButton>

class BrushButton: public QPushButton
{
public:
    void mouseReleaseEvent(QMouseEvent *);

    BrushButton(QWidget* = 0);
    ~BrushButton();
};


class PenColorButton: public QPushButton
{
public:
    void mouseReleaseEvent(QMouseEvent *);

    PenColorButton(QWidget* = 0);
    ~PenColorButton();
};


class TransparentBrushButton: public QPushButton
{
public:
    void mouseReleaseEvent(QMouseEvent *);

    TransparentBrushButton(QWidget* = 0);
    ~TransparentBrushButton();
};

#endif // COLORBUTTONS_H
