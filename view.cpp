#include "view.h"


View::View(QWidget *parent): QGraphicsView(parent)
{
    setMouseTracking(true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    show();
}

View::~View()
{

}
