#include "scene.h"
#include <QGraphicsSceneMouseEvent>
#include "shapes.h"

#include <iostream>
using namespace std;

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (action == BLOCKED)
    {
        return;
    }
    if (action == NONE)
    {
        checkButtons();
    }
    QPointF point;
    if (specialPoint->isVisible())
    {
        point = specialPoint->getCenter();
        if (specialPoint->getTransforming())
        {
            event->setScenePos(point);
            mouseMoveEvent(event);
        }
    }
    else
    {
        point = event->scenePos();
    }
    switch (action)
    {
    case ROTATING_SET_POINT:
    {
        item->setCentralPoint(point);
        item->setStartRotation();
        item->setRotatable(true);
        setAction(ROTATING);
        break;
    }
    case ROTATING:
    {
        item->setRotatable(false);
        item->norm();
        shape = UNKNOWN;
        setAction(NONE);
        break;
    }
    case MOVING:
    {
        item->setMovable(false);
        item->updateGrips();
        shape = UNKNOWN;
        setAction(NONE);
        break;
    }
    case RESIZING:
    {
        item->setResizable(false);
        item->updateGrips();
        shape = UNKNOWN;
        setAction(NONE);
        break;
    }
    case DRAWING:
    {
        switch (shape)
        {
        case CIRCLE:
        {
            item = new Circle(point.x(), point.y(), 0);
            addElement(item, shape);
            item->setResizable(true);
            break;
        }
        case ELLIPSE:
        {
            item = new Ellipse(point.x(), point.y(), 0, 0);
            addElement(item, shape);
            item->setResizable(true);
            break;
        }
        case RECTANGLE:
        {
            item = new Rectangle(point.x(), point.y(), 0, 0);
            addElement(item, shape);
            item->setResizable(true);
            break;
        }
        case LINE:
        {
            item = new Line(point.x(), point.y(), point.x(), point.y());
            addElement(item, shape);
            item->setResizable(true);
            break;
        }
        }
        setAction(RESIZING);
        break;
    }
    case CHOOSING_POINT:
    {
        setAction(BLOCKED);
        interpreter->gotPoint(point);
        break;
    }
    case NONE:
    {
        if (!scenePressed(event->scenePos()))
        {
            pressed = true;
            pressPoint = event->scenePos();
            selectingRect->setRect(-50, -50, 0, 0);     // ustawienie poza scena, niewidoczny
            selectingRect->setVisible(true);
        }
        break;
    }
    }
}


template <typename T>
void Scene::selectItems(const T &rec_or_point, bool precise)
{
    QList <Element*> selected_items;
    Element* elem;
    Shape* sh;
    foreach (QGraphicsItem *i, items())
    {
        if ((sh = dynamic_cast<Shape*> (i)) != NULL)
        {
            if (sh->trySelect(rec_or_point, precise))
            {
                elem = dynamic_cast<Element*> (i->topLevelItem());
                if (!selected_items.contains(elem))
                {
                    selected_items.append(elem);
                }
            }
        }
    }
    foreach (Element* e, selected_items)
    {
        e->check(!e->isChecked());
    }
}

void Scene::sceneClicked(const QPointF &p)
{
    selectItems(p, false);
}

bool Scene::scenePressed(const QPointF &point)
{
    Grip* grip;
    foreach (QGraphicsItem *i, items())
    {
        if (i->isVisible())
        {
            if ((grip = dynamic_cast<Grip*> (i)) != NULL)
            {
                if (grip->hasPointInside(point))
                {
                    grip->mousePressEvent();
                    return true;
                }
            }
        }
    }
    return false;
}

void Scene::deleteItems(QList<Element*> list)
{
    Element* elem;
    foreach (Element* e, list)
    {
        if (e->name() == GROUP)
        {
            foreach (QGraphicsItem *i, dynamic_cast<QGraphicsItem*>(e)->childItems())
            {
                if ((elem = dynamic_cast<Element*>(i)) != NULL)
                {
                    deleteElement(elem);
                }
            }
            deleteElement(e);
        }
        else
        {
            deleteElement(e);
        }
    }
}

void Scene::deleteAllItems()
{
    Element* elem;
    foreach (QGraphicsItem *i, items())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            delete elem;
        }
    }
}

void Scene::checkSpecialPoint(const QPointF &p)
{
    if (!findGripsNearby(p))
    {
        if (!findCutNearby(p))
        {
            specialPoint->setVisible(false);
        }
    }
}

bool Scene::findGripsNearby(const QPointF &p)
{
    Grip* grip;
    QGraphicsItem* qitem;
    try
    {
        qitem = dynamic_cast<QGraphicsItem*>(item);
    }
    catch (...)
    {
        qitem = NULL;
    }

    foreach (QGraphicsItem* i, items())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            if (!grip->hasInHierarchy(qitem) || !specialPoint->getTransforming())
            {
                if (grip->hasPointInside(p))
                {
                    specialPoint->setOnPoint(grip->rect().center());
                    specialPoint->setVisible(true);
                    return true;
                }
            }
        }
    }
    return false;
}

bool Scene::findCutNearby(QPointF p)
{
    QList<Shape*> shapes;
    Shape* sh;
    QGraphicsItem* qitem;
    try
    {
        qitem = dynamic_cast<QGraphicsItem*>(item);
    }
    catch (...)
    {
        qitem = NULL;
    }

    foreach (QGraphicsItem* i, items())
    {
        if ((sh = dynamic_cast<Shape*>(i)) != NULL)
        {
            if (!specialPoint->getTransforming() || !sh->hasInHierarchy(qitem))
            {
                if (sh->trySelect(p, false))
                {
                    shapes.append(sh);
                }
            }
        }
    }
    if (shapes.length() < 2)
    {
        return false;
    }
    float x = p.x(), y = p.y(), counter;
    for (int i = x - OUTLINE_THICKNESS; i < x + OUTLINE_THICKNESS; i++)
    {
        for (int j = y - OUTLINE_THICKNESS; j < y + OUTLINE_THICKNESS; j++)
        {
            counter = 0;
            p.setX(i);
            p.setY(j);
            foreach (Shape* shape, shapes)
            {
                if (shape->trySelect(p, true))
                {
                    counter++;
                }
            }
            if (counter >= 2)
            {
                specialPoint->setOnPoint(p);
                specialPoint->setVisible(true);
                return true;
            }
        }
    }
    return false;
}

void Scene::finishAction()
{
    switch (action)
    {
    case ROTATING:
    {
        item->setRotatable(false);
        item->norm();
        break;
    }
    case MOVING:
    {
        item->setMovable(false);
        item->updateGrips();
        break;
    }
    case RESIZING:
    {
        item->setResizable(false);
        item->updateGrips();
        break;
    }
    }
    shape = UNKNOWN;
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (action == BLOCKED)
    {
        return;
    }
    if (!pressed)
        // pressed == false - np. po skonczeniu rysowania, wybierania punktu
    {
        return;
    }
    pressed = false;
    selectingRect->setVisible(false);
    if (action == NONE && pressPoint.x() == event->scenePos().x() && pressPoint.y() == event->scenePos().y())
    {
        sceneClicked(pressPoint);
        return;
    }
    selectItems(selectingRect->rect(), selectingRect->getType());
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    float x = event->scenePos().x();
    float y = event->scenePos().y();
    QString napis("");
    napis.append(QString::number(x));
    napis.append(", ");
    napis.append(QString::number(y));
    l->setText(napis);

    checkSpecialPoint(event->scenePos());
    if (action == BLOCKED)
    {
        return;
    }
    if (action != NONE && action != CHOOSING_POINT)
    {
        switch (shape)
        {
        case CIRCLE:
        {
            dynamic_cast<Circle*>(item)->mouseMoveEvent(event);
            break;
        }
        case ELLIPSE:
        {
            dynamic_cast<Ellipse*>(item)->mouseMoveEvent(event);
            break;
        }
        case RECTANGLE:
        {
            dynamic_cast<Rectangle*>(item)->mouseMoveEvent(event);
            break;
        }
        case LINE:
        {
            dynamic_cast<Line*>(item)->mouseMoveEvent(event);
            break;
        }
        case GROUP:
        {
            dynamic_cast<Group*>(item)->mouseMoveEvent(event);
            break;
        }
        }
    }
    else
    {
        if (pressed)
        {
            selectingRect->mouseMoveEvent(pressPoint, event);
        }
    }
}

void Scene::keyPressEvent(QKeyEvent *event)
{
    if (action == BLOCKED || action == CHOOSING_POINT)
    {
        return;
    }
    switch (event->key())
    {
    case Qt::Key_Delete:
    {
        deleteItems(getCheckedElements());
        break;
    }
    case -1:
    {

    }
    }
}


void Scene::addElement(Element *e, SHAPES s)
{
    switch (s)
    {
    case CIRCLE:
    {
        addItem(dynamic_cast<QGraphicsEllipseItem*>(e));
        break;
    }
    case ELLIPSE:
    {
        addItem(dynamic_cast<QGraphicsEllipseItem*>(e));
        break;
    }
    case RECTANGLE:
    {
        addItem(dynamic_cast<QGraphicsRectItem*>(e));
        break;
    }
    case LINE:
    {
        addItem(dynamic_cast<QGraphicsLineItem*>(e));
        break;
    }
    case GROUP:
    {
        addItem(dynamic_cast<QGraphicsItemGroup*>(e));
        break;
    }
    }
}

void Scene::deleteElement(Element *elem)
{
    cout << "robie remove" << endl;
    removeItem(dynamic_cast<QGraphicsItem*>(elem));
    cout << "po remove" << endl;
    delete elem;
    cout << "po delete" << endl;
}

void Scene::removeElement(Element *elem)
{
    removeItem(dynamic_cast<QGraphicsItem*>(elem));
}

void Scene::createGroup(const QList<Element *> &checked_elements, const QPointF &point)
{
    Group *gr = new Group(point);
    foreach (Element *e, checked_elements)
    {
        removeElement(e);
        gr->addElement(e);
    }
    addElement(gr, gr->name());
}

void Scene::destroyGroup(const QList<Element*> &checked_elements)
{
    Group* group;
    Element* elem;
    foreach (Element *e, checked_elements)
    {
        if ((group = dynamic_cast<Group*>(e)) != NULL)
        {
            removeElement(e);
            foreach (QGraphicsItem* i, group->childItems())
            {
                if ((elem = dynamic_cast<Element*>(i)) != NULL)
                {
                    addElement(elem, elem->name());
                }
            }
            delete e;
        }
    }
}

ofstream &Scene::operator >>(ofstream &fs, Scene *s)
{
    
}

void Scene::setItemShape(SHAPES s)
{
    shape = s;
}

void Scene::setItem(Element *e)
{
    item = e;
}

void Scene::setAction(ACTIONS a)
{
    if (a == ROTATING || a == MOVING || a == RESIZING)
    {
        specialPoint->setTransforming(true);
    }
    else
    {
        if (a == BLOCKED || a == CHOOSING_POINT)
        {
            finishAction();
        }
        specialPoint->setTransforming(false);
    }
    action = a;
}

void Scene::checkAll(bool b)
{
    Element* elem;
    foreach (QGraphicsItem *g, items())
    {
        if ((elem = dynamic_cast<Element*>(g)) != NULL)
        {
             elem->check(b);
        }
    }
}

QList <Element*> Scene::getCheckedElements()
{
    QList <Element*> list;
    Element* elem;
    foreach (QGraphicsItem* i, items())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            if (elem->isChecked())
            {
                list.append(elem);
            }
        }
    }
    return list;
}

void Scene::setInterpreter(Interpreter *i)
{
    interpreter = i;
}

void Scene::checkButtons()
{
    if (b->isChecked())
    {
        setAction(DRAWING);
        shape = ELLIPSE;
    }
}


Scene::Scene(Button* _b, QLabel *_l): shape(UNKNOWN), item(NULL), action(NONE), pressed(false)
{
    l = _l;
    b = _b;
    setSceneRect(0, 0, 1050, 650);
    selectingRect = new SelectingRect(0, 0, 0, 0);
    specialPoint = new SpecialPoint();
    addItem(selectingRect);
    addItem(specialPoint);
}

Scene::~Scene()
{
    cout << "usuwam scene" << endl;
    delete selectingRect;
    delete specialPoint;
    deleteAllItems();
}


QRectF Scene::SelectingRect::calcResizing(const QPointF &mouse, const QPointF &base)
{
    float width = mouse.x() - base.x();
    float height = mouse.y() - base.y();
    float corner_x, corner_y;
    if (width < 0)
    {
        width *= -1;
        corner_x = mouse.x();
        setBrush(Qt::green);
    }
    else
    {
        corner_x = base.x();
        setBrush(Qt::blue);
    }
    if (height < 0)
    {
        height *= -1;
        corner_y = mouse.y();
    }
    else
    {
        corner_y = base.y();
    }
    return QRectF(corner_x, corner_y, width, height);
}

void Scene::SelectingRect::mouseMoveEvent(const QPointF &origin, QGraphicsSceneMouseEvent *event)
{
    setRect(calcResizing(event->scenePos(), origin));
}

bool Scene::SelectingRect::getType()
{
    if (brush() == Qt::blue)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Scene::SelectingRect::SelectingRect(const float &_x, const float &_y, const float &_w, const float &_h)
    :QGraphicsRectItem(_x, _y, _w, _h)
{
    setVisible(false);
    setBrush(Qt::blue);
    setOpacity(0.5);
}

Scene::SelectingRect::~SelectingRect()
{

}


void Scene::SpecialPoint::setOnPoint(const QPointF &p)
{
    setX(p.x() - SPECIAL_POINT_SIZE / 2);
    setY(p.y() - SPECIAL_POINT_SIZE / 2);
}

void Scene::SpecialPoint::setTransforming(bool b)
{
    transforming = b;
}

bool Scene::SpecialPoint::getTransforming()
{
    return transforming;
}

QPointF Scene::SpecialPoint::getCenter()
{
    return QPointF(x() + SPECIAL_POINT_SIZE / 2, y() + SPECIAL_POINT_SIZE / 2);
}

Scene::SpecialPoint::SpecialPoint(): transforming(false)
{
    setZValue(2);
    QPolygon p;
    p.append(QPoint(0, 1));
    p.append(QPoint(1, 0));
    p.append(QPoint(5, 4));
    p.append(QPoint(9, 0));
    p.append(QPoint(10, 1));
    p.append(QPoint(6, 5));
    p.append(QPoint(10, 9));
    p.append(QPoint(9, 10));
    p.append(QPoint(5, 6));
    p.append(QPoint(1, 10));
    p.append(QPoint(0, 9));
    p.append(QPoint(4, 5));
    QPen pen(QColor(0, 0, 255));
    setPen(pen);
    setBrush(Qt::blue);
    setPolygon(p);
    setVisible(false);
}

Scene::SpecialPoint::~SpecialPoint()
{

}


ofstream& operator <<(ofstream &fs, Scene *s)
{
    Element* elem;
    foreach (QGraphicsItem *i, s->items())
    {
        elem = dynamic_cast<Element*>(i);
        if (elem != NULL)
        {
            if (i->parentItem() == NULL)
            {
                fs << elem;
            }
        }
    }
    return fs;
}
