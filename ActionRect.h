#ifndef SPECIALRECT_H
#define SPECIALRECT_H

#include <QGraphicsRectItem>
#include "point.h"

class Grip: public QGraphicsRectItem
{
protected:
    int id;

public:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent*) = 0;

    bool hasPointInside(Point);
    int getId(void);
    bool hasInHierarchy(QGraphicsItem*);

    Grip(QGraphicsItem* = 0);
    ~Grip();

    const static int GRIP_SIZE = 10;

    void move(const double &, const double &);

};

class BorderGrip: public Grip
{
public:
    BorderGrip(const int &, QGraphicsItem* = 0);
    ~BorderGrip();

    void mousePressEvent(QGraphicsSceneMouseEvent*);
};

class CentralGrip: public Grip
{

public:
    CentralGrip(const int &, QGraphicsItem* = 0);
    ~CentralGrip();

    void mousePressEvent(QGraphicsSceneMouseEvent*);

};

#endif // SPECIALRECT_H
