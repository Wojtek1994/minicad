#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "view.h"
#include "scene.h"
#include "shapebuttons.h"
#include "interpreter.h"
#include "iobuttons.h"
#include "colorbuttons.h"

#include <QMainWindow>
#include <QLabel>


class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    View *view;
    Scene *scene;
    Interpreter *interpreter;
    ShapeButtons *shapeButtons;
    SaveButton *saveButton;
    OpenButton *openButton;
    BrushButton *brushButton;
    TransparentBrushButton *transparentBrushButton;
    PenColorButton *penColorButton;
    QLabel *coordsLabel;
    QLabel *specialCoordsLabel;

public:
    Scene *getScene(void);
    Interpreter *getInterpreter(void);
    ShapeButtons *getShapeButtons(void);
    QLabel *getCoordsLabel(void);
    QLabel *getSpecialCoordsLabel(void);

    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static const int VIEW_X = 10;
    static const int VIEW_Y = 10;
    static const int MIN_VIEW_WIDTH = 200;
    static const int MIN_VIEW_HEIGHT = 200;
    static const int BUTTONS_WIDTH = 150;
    static const int INTERPRETER_HEIGHT = 300;
    static const int TEXTBUFFER_HEIGHT = 170;
    static const int VERTICAL_GAP = 7;
    static const int BIG_VERTICAL_GAP = 30;
    static const int HORIZONTAL_GAP = 10;
    static const int BUTTON_HEIGHT = 25;
    static const int BUTTON_WIDTH = 100;
    static const int LABEL_HEIGHT = 20;
    static const int EDIT_HEIGHT = 25;

};


#endif // MAINWINDOW_H
