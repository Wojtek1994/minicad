#include "MainWindow.h"

#include <QMessageBox>
#include <Windows.h>

Scene *MainWindow::getScene()
{
    return scene;
}

Interpreter *MainWindow::getInterpreter()
{
    return interpreter;
}

ShapeButtons *MainWindow::getShapeButtons()
{
    return shapeButtons;
}

QLabel *MainWindow::getCoordsLabel()
{
    return coordsLabel;
}

QLabel *MainWindow::getSpecialCoordsLabel()
{
    return specialCoordsLabel;
}

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
  // ustawienie wszystkich obiektow w oknie glownym
{
    showFullScreen();
    int horizontal = geometry().width();
    int vertical = geometry().height();
    showMaximized();

    scene = new Scene(this);
    view = new View(this);
    view->setGeometry(VIEW_X, VIEW_Y, max(MIN_VIEW_WIDTH, horizontal - BUTTONS_WIDTH), max(MIN_VIEW_HEIGHT, vertical - INTERPRETER_HEIGHT));
    view->setScene(scene);

    interpreter = new Interpreter(this);
    interpreter->setRect(QRect(VIEW_X, view->geometry().height() + VIEW_Y + VERTICAL_GAP, max(horizontal - BUTTONS_WIDTH, MIN_VIEW_WIDTH), INTERPRETER_HEIGHT));

    int x = VIEW_X + view->geometry().width() + HORIZONTAL_GAP;

    shapeButtons = new ShapeButtons(this);
    shapeButtons->setGeometry(x, VIEW_Y, BUTTON_WIDTH, 4 * BUTTON_HEIGHT + 3 * VERTICAL_GAP);

    int y = VIEW_Y + shapeButtons->geometry().height() + VERTICAL_GAP + BIG_VERTICAL_GAP;

    brushButton = new BrushButton(this);
    brushButton->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    y += BUTTON_HEIGHT + VERTICAL_GAP;

    penColorButton = new PenColorButton(this);
    penColorButton->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    y += BUTTON_HEIGHT + VERTICAL_GAP;

    transparentBrushButton = new TransparentBrushButton(this);
    transparentBrushButton->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    y += BUTTON_HEIGHT + VERTICAL_GAP + BIG_VERTICAL_GAP;


    coordsLabel = new QLabel(this);
    coordsLabel->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    coordsLabel->setVisible(true);
    y += BUTTON_HEIGHT + VERTICAL_GAP;

    specialCoordsLabel = new QLabel(this);
    specialCoordsLabel->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    specialCoordsLabel->setVisible(true);
    y += BUTTON_HEIGHT + VERTICAL_GAP + BIG_VERTICAL_GAP;

    saveButton = new SaveButton(this);
    saveButton->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    y += BUTTON_HEIGHT + VERTICAL_GAP;

    openButton = new OpenButton(this);
    openButton->setGeometry(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);

}


MainWindow::~MainWindow()
{
    delete scene;
    delete view;
    delete interpreter;
    delete shapeButtons;
    delete brushButton;
    delete penColorButton;
    delete transparentBrushButton;
    delete coordsLabel;
    delete specialCoordsLabel;
    delete saveButton;
    delete openButton;
}
