#include "shapes.h"
#include "scene.h"
#include <cmath>

#include <iostream>
using namespace std;


bool Element::isResizable()
{
    return resizable;
}

bool Element::isRotatable()
{
    return rotatable;
}

bool Element::isMovable()
{
    return movable;
}

bool Element::isChecked()
{
    return checked;
}


void Element::childId(const int &_id)
{
    id = _id;
}


double Element::calculateAngle(const Point &center, const Point &current)
{
    // zwraca kat miedzy dwoma punktami (punkt center - teoretyczny poczatek ukladu wsp.
    // zwracany kat jest zgodny z ruchem wskazowek zegara (podobnie jak funkcja setRotation
    double dist;
    if ((dist = current.distance(center)) == 0)
    {
        return 0;
    }
    double angle = safeAcos((current.x() - center.x()) / dist);
    if (center.y() > current.y())
    {
        angle *= -1;
    }
    return angle;
}

Point Element::calculateRotatedPoint(Point center, Point rot, const double &angle)
{
    center.setX(center.x() - rot.x());
    center.setY(center.y() - rot.y());
    double x = center.x() * cos(angle) - center.y() * sin(angle);
    double y = center.x() * sin(angle) + center.y() * cos(angle);
    return Point(x + rot.x(), y + rot.y());
}

double Element::safeAcos(const double &f)
{
    if (f >= 1)
    {
        return 0;
    }
    else
    {
        if (f <= -1)
        {
            return -PI;
        }
        else
        {
            return acos(f);
        }
    }
}

bool Element::hasInHierarchy(QGraphicsItem *i)
{
    QGraphicsItem* qthis = dynamic_cast<QGraphicsItem*>(this);
    while (qthis)
    {
        if (qthis == i)
        {
            return true;
        }
        else
        {
            qthis = qthis->parentItem();
        }
    }
    return false;
}

Element::Element(): resizable(false), rotatable(false), movable(false), checked(false), id(-1)
{
}

Element::~Element()
{
}





Shape::Shape()
{
}

Shape::~Shape()
{
}

void Shape::setRotatable(bool b)
{
    rotatable = b;
}

std::vector<Point>& Shape::findCuts(Shape *s1, Shape *s2)
{
    std::vector<Point> points;
    switch (s1->name())
    {
    case CIRCLE:
    {
        break;
    }
    case ELLIPSE:
    {
        break;
    }
    case RECTANGLE:
    {
        break;
    }
    case LINE:
    {
        break;
    }
    default:
    {
        return points;
    }
    }
    return points;
}

void Shape::setMovable(bool b)
{
    movable = b;
}


void Shape::setCentralPoint(const Point &point)
{
    central_point = point;
}

void Shape::setStartRotation()
{
    start_rotation = dynamic_cast<QGraphicsItem*>(this)->rotation();
}

double Shape::getTotalRotationAngle()
{
    return (dynamic_cast<QGraphicsItem*>(this)->rotation() - start_rotation) * PI / 180;
}

void Shape::check(bool b)
{
    checked = b;
    foreach (QGraphicsItem* i, dynamic_cast<QGraphicsItem*>(this)->childItems())
    {
        i->setVisible(b);
    }
    setLinesStyle(b);
}

void Shape::updateGrips()
{
    Grip* grip;
    QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(this);
    foreach (QGraphicsItem* i, item->childItems())
    {
        grip = dynamic_cast<Grip*>(i);
        grip->setRect(calcPosition(grip->getId()));
    }
}

void Shape::norm()
{
    QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(this);
    foreach (QGraphicsItem *i, item->childItems())
    {
        i->setRotation(-item->rotation());
    }
    updateGrips();
}

double Shape::closestX(const QRectF & rec, const Point &p)
{
    if (p.x() < rec.x())
    {
        return rec.x();
    }
    else
    {
        if (p.x() > rec.x() + rec.width())
        {
            return rec.x() + rec.width();
        }
        else
        {
            return p.x();
        }
    }
}

double Shape::closestY(const QRectF &rec, const Point &p)
{
    if (p.y() < rec.y())
    {
        return rec.y();
    }
    else
    {
        if (p.y() > rec.y() + rec.height())
        {
            return rec.y() + rec.height();
        }
        else
        {
            return p.y();
        }
    }
}

Ellipse::Ellipse(const double &_x, const double &_y, const double &_w, const double &_h):QGraphicsEllipseItem(_x, _y, _w, _h)
{
    for (int i = 0; i < ELLIPSE_BORDER_GRIP_NUM + ELLIPSE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < ELLIPSE_BORDER_GRIP_NUM)
        {
            new BorderGrip(_x, _y, i, this);
        }
        else
        {
            new CentralGrip(_x, _y, i, this);
        }
    }
    setVisible(true);
}

Ellipse::Ellipse(): QGraphicsEllipseItem()
{
    for (int i = 0; i < ELLIPSE_BORDER_GRIP_NUM + ELLIPSE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < ELLIPSE_BORDER_GRIP_NUM)
        {
            new BorderGrip(i, this);
        }
        else
        {
            new CentralGrip(i, this);
        }
    }
    setVisible(true);
}

Ellipse::~Ellipse()
{
}

void Ellipse::setResizable(bool b)
{
    resizable = b;
}

void Ellipse::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(Qt::DashLine);
    }
    else
    {
        setPen(Qt::SolidLine);
    }
}

SHAPES Ellipse::name()
{
    return ELLIPSE;
}

bool Ellipse::trySelect(const Point &p, bool precise)
{
    Point point = mapFromScene(p);
    QRectF rec = rect();
    Point center = rec.center();
    double d = sqrt(abs(rec.width() * rec.width() - rec.height() * rec.height())) / 2;
    Point f1, f2;
    double semi_major_axis;
    if (rec.width() > rec.height())
    {
        f1.setY(center.y());
        f2.setY(center.y());
        f1.setX(center.x() - d);
        f2.setX(center.x() + d);
        semi_major_axis = rec.width();
    }
    else
    {
        f1.setY(center.y() - d);
        f2.setY(center.y() + d);
        f1.setX(center.x());
        f2.setX(center.x());
        semi_major_axis = rec.height();
    }
    double dist = abs(point.distance(f1) + point.distance(f2) - semi_major_axis);
    if (precise)
    {
        return dist < OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        return dist < OUTLINE_THICKNESS;
    }
}

bool Ellipse::trySelect(const QRectF &rectangle, bool whole)
{
    QRectF rec = rect();
    Point center = rec.center();
    double d = sqrt(abs(rec.width() * rec.width() - rec.height() * rec.height())) / 2;
    Point f1, f2;
    double semi_major_axis;
    if (rec.width() > rec.height())
    {
        f1.setY(center.y());
        f2.setY(center.y());
        f1.setX(center.x() - d);
        f2.setX(center.x() + d);
        semi_major_axis = rec.width();
    }
    else
    {
        f1.setY(center.y() - d);
        f2.setY(center.y() + d);
        f1.setX(center.x());
        f2.setX(center.x());
        semi_major_axis = rec.height();
    }

    f1 = mapToScene(f1);
    f2 = mapToScene(f2);
    center = mapToScene(center);


    Point closest_point(closestX(rectangle, center), closestY(rectangle, center));

    if (whole)
    {
        double x_dist = 0;
        double y_dist = 0;
        return rectangle.contains(QRectF(center.x() - x_dist, center.y() - y_dist, 2 * x_dist, 2 * y_dist));
    }
    else
    {
        if (closest_point.distance(f1) + closest_point.distance(f2) > semi_major_axis)
        {
            cout << "zle" << endl;
            return false;
        }
        else
        {
            return (f1.distance(rectangle.topRight()) + f2.distance(rectangle.topRight()) > semi_major_axis) ||
                   (f1.distance(rectangle.topLeft()) + f2.distance(rectangle.topLeft()) > semi_major_axis) ||
                   (f1.distance(rectangle.bottomRight()) + f2.distance(rectangle.bottomRight()) > semi_major_axis) ||
                   (f1.distance(rectangle.bottomLeft()) + f2.distance(rectangle.bottomLeft()) > semi_major_axis);
        }
    }
}


QRectF Ellipse::calcPosition(int i)
{
    Point point;
    QRectF rec = rect();
    double width = rec.width(), height = rec.height(), x = rec.x(), y = rec.y();
    switch (i)
    {
    case TOP:
    {
        point.setX(x + width / 2);
        point.setY(y);
        break;
    }
    case DOWN:
    {
        point.setX(x + width / 2);
        point.setY(y + height);
        break;
    }
    case LEFT:
    {
        point.setX(x);
        point.setY(y + height / 2);
        break;
    }
    case RIGHT:
    {
        point.setX(x + width);
        point.setY(y + height / 2);
        break;
    }
    case CENTER:
    {
        point.setX(x + width / 2);
        point.setY(y + height / 2);
        break;
    }
    default:
    {
        point.setX(0);
        point.setY(0);
        // nieznana pozycja
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}


void Ellipse::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        QRectF rec = rect();
        Point center = rec.center();

        switch (id)
        {
        case TOP:
        case DOWN:
        {
            double new_dimension = mouse.distance(center);
            setRect(rec.x(), center.y() - new_dimension, rec.width(), 2 * new_dimension);
            break;
        }
        case LEFT:
        case RIGHT:
        {
            double new_dimension = mouse.distance(center);
            setRect(center.x() - new_dimension, rec.y(), 2 * new_dimension, rec.height());
            break;
        }
        default:
        {
            // uzywane przy tworzeniu nowego obiektu
            double half_width = abs(mouse.x() - center.x());
            double half_height = abs(mouse.y() - center.y());
            setRect(center.x() - half_width, center.y() - half_height, 2 * half_width, 2 * half_height);
        }
        }
    }
    if (isMovable())
    {
        Point center = rect().center();
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = calculateAngle(central_point, event->scenePos());
        rotate(angle);
    }
}

void Ellipse::move(const double &x, const double &y)
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Ellipse::rotate(const double &angle)
{
    QRectF rec = rect();
    Point corner = calculateRotatedPoint(mapToScene(rec.topLeft()), central_point, angle + (start_rotation - rotation()) * PI / 180);
    setRotation(start_rotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}



Rectangle::Rectangle(const double &_x, const double &_y, const double &_w, const double &_h)
    : QGraphicsRectItem(_x, _y, _w, _h)
{
    for (int i = 0; i < RECTANGLE_BORDER_GRIP_NUM + RECTANGLE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < RECTANGLE_BORDER_GRIP_NUM)
        {
            new BorderGrip(_x, _y, i, this);
        }
        else
        {
            new CentralGrip(_x, _y, i, this);
        }
    }
    setVisible(true);
}

Rectangle::Rectangle(): QGraphicsRectItem()
{
    for (int i = 0; i < RECTANGLE_BORDER_GRIP_NUM + RECTANGLE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < RECTANGLE_BORDER_GRIP_NUM)
        {
            new BorderGrip(i, this);
        }
        else
        {
            new CentralGrip(i, this);
        }
    }
    setVisible(true);
}

Rectangle::~Rectangle()
{
}

void Rectangle::setResizable(bool b)
{
    resizable = b;
    QRectF rec = rect();
    switch (id)
    {
    case TOPL:
    {
        central_point.setX(rec.x() + rec.width());
        central_point.setY(rec.y() + rec.height());
        break;
    }
    case DOWNL:
    {
        central_point.setX(rec.x() + rec.width());
        central_point.setY(rec.y());
        break;
    }
    case TOPR:
    {
        central_point.setX(rec.x());
        central_point.setY(rec.y() + rec.height());
        break;
    }
    case DOWNR:
    {
        central_point.setX(rec.x());
        central_point.setY(rec.y());
        break;
    }
    default:
    {
        // tylko przy tworzeniu nowego obiektu
        central_point.setX(rec.x());
        central_point.setY(rec.y());
    }
    }
}

void Rectangle::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(Qt::DashLine);
    }
    else
    {
        setPen(Qt::SolidLine);
    }
}


SHAPES Rectangle::name()
{
    return RECTANGLE;
}

bool Rectangle::trySelect(const Point &point, bool precise)
{
    double outline;
    if (precise)
    {
        outline = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        outline = OUTLINE_THICKNESS;
    }
    Point p = mapFromScene(point);
    QRectF rec = rect();
    if ((abs(rec.x() - p.x()) < outline) ||
        (abs(rec.x() + rec.width() - p.x()) < outline))
    {
        return rec.y() - outline < p.y() && p.y() < rec.y() + rec.height() + outline;
    }
    if ((abs(rec.y() - p.y()) < outline) ||
        (abs(rec.y() + rec.height() - p.y()) < outline))
    {
        return rec.x() - outline < p.x() && p.x() < rec.x() + rec.width() + outline;
    }
    return false;
}

bool Rectangle::trySelect(const QRectF& rec, bool whole)
{
//    dx1 = Left-A.x; dx2 = Right-A.x; // Calculate difference
//    ext1 = A.y;
//    // If A.x is within x=Left and x=Right, this is the exterimity,
//    // otherwise, it will be used to calculate dy (= A.y-B.y or A.y-(-B.y) = ext-B.y or ext+B.y).

//    if (dx1*dx2 > 0)
//    {
//     dx = A.x;
//         // dx = (A.x-B.x),    dy = (A.y-B.y),    dx1 = (Right-A.x)
//     if (dx1 < 0) { dx -= B.x; ext1 -= B.y; dx1 = dx2; }
//         // dx = (A.x-(-B.x)), dy = (A.y-(-B.y))  dx1 = (Left-A.x)
//     else         { dx += B.x; ext1 += B.y; }
//         // ext1 = A.y + (dy/dx)*dx1
//     ext1 *= dx1; ext1 /= dx; ext1 += A.y;
//    }


    QRectF re1 = rect();
    Point tl = mapToScene(rect().topLeft());
    Point br = mapToScene(rect().bottomRight());
    QRectF re = rec.intersected(QRectF(tl, br));
    cout << rec.x() << " " << rec.y() << " " << rec.x() + rec.width() << " "
         << rec.y() + rec.height() << " " << rec.width() << " " << rec.height() << endl;
    cout << re.x() << " " << re.y() << " " << re.x() + re.width() << " "
         << re.y() + re.height() << " " << re.width() << " " << re.height() << endl;

    return false;
}


QRectF Rectangle::calcPosition(int i)
{
    Point point;
    QRectF rec = rect();
    double width = rec.width(), height = rec.height(), x = rec.x(), y = rec.y();
    switch (i)
    {
    case TOPL:
    {
        point.setX(x);
        point.setY(y);
        break;
    }
    case DOWNL:
    {
        point.setX(x);
        point.setY(y + height);
        break;
    }
    case TOPR:
    {
        point.setX(x + width);
        point.setY(y);
        break;
    }
    case DOWNR:
    {
        point.setX(x + width);
        point.setY(y + height);
        break;
    }
    case CENTER:
    {
        point.setX(x + width / 2);
        point.setY(y + height / 2);
        break;
    }
    default:
    {
        point.setX(0);
        point.setY(0);
        // nieznana pozycja
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}


QRectF Rectangle::calcResizing(Point mouse, Point base)
{
    double width = mouse.x() - base.x();
    double height = mouse.y() - base.y();
    double corner_x, corner_y;
    if (width < 0)
    {
        width *= -1;
        corner_x = mouse.x();
    }
    else
    {
        corner_x = base.x();
    }
    if (height < 0)
    {
        height *= -1;
        corner_y = mouse.y();
    }
    else
    {
        corner_y = base.y();
    }
    return QRectF(corner_x, corner_y, width, height);
}

void Rectangle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    Point center = rect().center();
    if (isResizable())
    {
        setRect(calcResizing(mouse, central_point));
    }
    if (isMovable())
    {
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = calculateAngle(central_point, event->scenePos());
        rotate(angle);
    }
}

void Rectangle::move(const double &x, const double &y)
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Rectangle::rotate(const double &angle)
{
    QRectF rec = rect();
    Point corner = calculateRotatedPoint(mapToScene(rec.topLeft()), central_point, angle + (start_rotation - rotation()) * PI / 180);
    setRotation(start_rotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}


Line::Line(const double &_x1, const double &_y1, const double &_x2, const double &_y2)
    : QGraphicsLineItem(_x1, _y1, _x2, _y2)
{
    for (int i = 0; i < LINE_BORDER_GRIP_NUM + LINE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < LINE_BORDER_GRIP_NUM)
        {
            new BorderGrip(_x1, _y1, i, this);
        }
        else
        {
            new CentralGrip(_x1, _y1, i, this);
        }
    }
    setVisible(true);
}

Line::Line(): QGraphicsLineItem()
{
    for (int i = 0; i < LINE_BORDER_GRIP_NUM + LINE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < LINE_BORDER_GRIP_NUM)
        {
            new BorderGrip(i, this);
        }
        else
        {
            new CentralGrip(i, this);
        }
    }
    setVisible(true);
}

Line::~Line()
{
}

void Line::setResizable(bool b)
{
    resizable = b;
}

void Line::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(Qt::DashLine);
    }
    else
    {
        setPen(Qt::SolidLine);
    }
}


SHAPES Line::name()
{
    return LINE;
}

bool Line::trySelect(const Point &point, bool precise)
{
    double outline;
    if (precise)
    {
        outline = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        outline = OUTLINE_THICKNESS;
    }
    Point p2, p = mapFromScene(point);
    QLineF l = line();
    double x1 = l.x1(), x2 = l.x2(), y1 = l.y1(), y2 = l.y2();
    double dx = x2 - x1;
    double dy = y2 - y1;
    Point pmin, pmax;
    x1 < x2 ? (pmin = Point(x1, y1), pmax = Point(x2, y2)) : (pmin = Point(x2, y2), pmax = Point(x1, y1));

    if (dx == 0 && dy == 0)
    {
        return p.distance(pmin) < outline;
    }
    else
    {
        double u = ((p.x() - x1) * dx + (p.y() - y1) * dy) / (dx * dx + dy * dy);
        p2.setX(x1 + u * dx);
        p2.setY(y1 + u * dy);
    }

    if (p2.x() < pmin.x())
    {
        return p.distance(pmin) < outline;
    }
    else
    {
        if (p2.x() > pmax.x())
        {
            return p.distance(pmax) < outline;
        }
        else
        {
            return p.distance(p2) < outline;
        }
    }
}

bool Line::trySelect(const QRectF&, bool whole)
{
    return false;
}

QRectF Line::calcPosition(int i)
{
    QRectF rec;
    QLineF lin = line();
    Point point;
    switch (i)
    {
    case BEGIN:
    {
        point = lin.p1();
        break;
    }
    case END:
    {
        point = lin.p2();
        break;
    }
    case CENTER:
    {
        point.setX((lin.x1() + lin.x2()) / 2);
        point.setY((lin.y1() + lin.y2()) / 2);
        break;
    }
    default:
    {
        point.setX(0);
        point.setY(0);
        // nieznana pozycja
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}


void Line::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QLineF lin = line();
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        switch (id)
        {
        case BEGIN:
        {
            setLine(mouse.x(), mouse.y(), lin.x2(), lin.y2());
            break;
        }
        case END:
        {
            setLine(lin.x1(), lin.y1(), mouse.x(), mouse.y());
            break;
        }
        default:
        {
            // uzywane przy tworzeniu nowego obiektu
            setLine(lin.x1(), lin.y1(), mouse.x(), mouse.y());
        }
        }
    }
    if (isMovable())
    {
        double dx = mouse.x() - (lin.x1() + lin.x2()) / 2;
        double dy = mouse.y() - (lin.y1() + lin.y2()) / 2;
        move(dx, dy);
    }
    if (isRotatable())
    {
        double angle = calculateAngle(central_point, event->scenePos());
        rotate(angle);
    }
}

void Line::move(const double &x, const double &y)
{
    QLineF l = line();
    setLine(l.x1() + x, l.y1() + y, l.x2() + x, l.y2() + y);
}

void Line::rotate(const double &angle)
{
    QLineF l = line();
    Point begin(l.x1(), l.y1());
    begin = calculateRotatedPoint(mapToScene(begin), central_point, angle + (start_rotation - rotation()) * PI / 180);
    setRotation(start_rotation + angle * 180 / PI);
    begin = mapFromScene(begin);
    setLine(begin.x(), begin.y(), begin.x() + l.dx(), begin.y() + l.dy());
}


Circle::Circle(const double &_x, const double &_y, const double &_r)
    : QGraphicsEllipseItem(_x, _y, _r, _r)
{
    cout << "tworze kolko" << endl;
    for (int i = 0; i < CIRCLE_BORDER_GRIP_NUM + CIRCLE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < CIRCLE_BORDER_GRIP_NUM)
        {
            new BorderGrip(_x, _y, i, this);
        }
        else
        {
            new CentralGrip(_x, _y, i, this);
        }
    }
    setVisible(true);
}

Circle::Circle(): QGraphicsEllipseItem()
{
    for (int i = 0; i < CIRCLE_BORDER_GRIP_NUM + CIRCLE_CENTRAL_GRIP_NUM; i++)
    {
        if (i < CIRCLE_BORDER_GRIP_NUM)
        {
            new BorderGrip(i, this);
        }
        else
        {
            new CentralGrip(i, this);
        }
    }
    setVisible(true);
}

Circle::~Circle()
{
    cout << "usuwam kolko" << endl;
}

void Circle::setResizable(bool b)
{
    resizable = b;
}

void Circle::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(Qt::DashLine);
    }
    else
    {
        setPen(Qt::SolidLine);
    }
}


SHAPES Circle::name()
{
    return CIRCLE;
}

bool Circle::trySelect(const Point &p, bool precise)
{
    Point point = mapFromScene(p);
    QRectF rec = rect();
    Point center = rec.center();
    double radius = rec.width() / 2;
    double precision;
    if (precise)
    {
        precision = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        precision = OUTLINE_THICKNESS;
    }
    if (abs(point.distance(center) - radius) < precision)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Circle::trySelect(const QRectF &rec, bool whole)
{
    Point circle_center = mapToScene(rect().center());
    double radius = rect().width() / 2;
    double x = closestX(rec, circle_center);
    double y = closestY(rec, circle_center);

    if (whole)
    {
        return rec.contains(QRectF(circle_center.x() - radius, circle_center.y() - radius, 2 * radius, 2 * radius));
    }
    else
    {
        if (circle_center.distance(Point(x, y)) > radius)
        {
            return false;
        }
        else
        {
            return (circle_center.distance(rec.topRight()) > radius) ||
                   (circle_center.distance(rec.topLeft()) > radius) ||
                   (circle_center.distance(rec.bottomRight()) > radius) ||
                   (circle_center.distance(rec.bottomLeft()) > radius);
        }
    }
}


QRectF Circle::calcPosition(int i)
{
    QRectF rec = rect();
    double width = rec.width(), height = rec.height();
    Point point = mapToScene(rec.center());
    switch (i)
    {
    case TOP:
    {
        point.setX(point.x());
        point.setY(point.y() - height / 2);
        break;
    }
    case DOWN:
    {
        point.setX(point.x());
        point.setY(point.y() + height / 2);
        break;
    }
    case LEFT:
    {
        point.setX(point.x() - width / 2);
        point.setY(point.y());
        break;
    }
    case RIGHT:
    {
        point.setX(point.x() + width / 2);
        point.setY(point.y());
        break;
    }
    case CENTER:
    {
        point.setX(point.x());
        point.setY(point.y());
        break;
    }
    default:
    {
        point.setX(0);
        point.setY(0);
        // nieznana pozycja
    }
    }
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}

void Circle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        QRectF rec = rect();
        Point center = rec.center();
        double new_radius = mouse.distance(center);
        setRect(center.x() - new_radius, center.y() - new_radius, 2 * new_radius, 2 * new_radius);
    }
    if (isMovable())
    {
        Point center = rect().center();
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = calculateAngle(central_point, event->scenePos());
        rotate(angle);
    }
}

void Circle::move(const double &x, const double &y)
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Circle::rotate(const double &angle)
{
    QRectF rec = rect();
    Point corner = calculateRotatedPoint(mapToScene(rec.topLeft()), central_point, angle + (start_rotation - rotation()) * PI / 180);
    setRotation(start_rotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}


Group::Group(Point point): QGraphicsItemGroup()
{
    cout << "tworze grupe" << endl;
    new CentralGrip(point.x() - Grip::GRIP_SIZE / 2, point.y() - Grip::GRIP_SIZE / 2, 0, this);
    groupPoint = point;
    setVisible(true);
}

Group::Group()
{
    new CentralGrip(0, this);
    setVisible(true);
}

Group::~Group()
{
    cout << "usuwam grupe" << endl;
    foreach (QGraphicsItem* i, childItems())
    {
        if (dynamic_cast<Grip*>(i) != NULL)
        {
            delete i;
        }
    }
}

void Group::setMovable(bool b)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setMovable(b);
        }
    }
    movable = b;
}

void Group::setRotatable(bool b)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setRotatable(b);
        }
    }
    rotatable = b;
}

void Group::setResizable(bool b)
{
    // nie mozna zmieniac rozmiaru grupy
}

void Group::setLinesStyle(bool b)
{
    linesStyle = b;
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setLinesStyle(b);
        }
    }
}

void Group::check(bool b)
{
    checked = b;
    Grip* grip;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            grip->setVisible(b);
            break;
        }
    }
    setLinesStyle(b);
}

void Group::norm()
{
    Element* elem;
    Shape* sh;
    double angle;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->norm();
        }
        if ((sh = dynamic_cast<Shape*>(i)) != NULL)
        {
            angle = sh->getTotalRotationAngle();
        }
    }
    groupPoint = calculateRotatedPoint(groupPoint, central_point, angle);
    updateGrips();
}

SHAPES Group::name()
{
    return GROUP;
}

void Group::setCentralPoint(const Point &point)
{
    Element* elem;
    central_point = point;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setCentralPoint(point);
        }
    }
}

void Group::setStartRotation()
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setStartRotation();
        }
    }
}

void Group::updateGrips()
{
    Grip* grip;
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            grip->setRect(groupPoint.x() - Grip::GRIP_SIZE / 2, groupPoint.y() - Grip::GRIP_SIZE / 2, Grip::GRIP_SIZE, Grip::GRIP_SIZE);
        }
        else
        {
            if ((elem = dynamic_cast<Element*>(i)) != NULL)
            {
                elem->updateGrips();
            }
        }
    }
}


void Group::addElement(Element *e)
{
    addToGroup(dynamic_cast<QGraphicsItem*>(e));
}

Point Group::getGroupPoint()
{
    return groupPoint;
}

void Group::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isRotatable())
    {
        double angle = calculateAngle(central_point, event->scenePos());
        rotate(angle);
    }
    if (isMovable())
    {
        move(event->scenePos().x() - groupPoint.x(), event->scenePos().y() - groupPoint.y());
    }
}


void Group::move(const double &x, const double &y)
{
    Element* elem;
    Point child_move;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            child_move = mapToItem(i, Point(x, y));
            elem->move(child_move.x(), child_move.y());
        }
    }
    groupPoint.setX(groupPoint.x() + x);
    groupPoint.setY(groupPoint.y() + y);
}

void Group::rotate(const double &angle)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->rotate(angle);
        }
    }
}


ofstream &operator <<(ofstream &fs, Element *e)
{
    switch(e->name())
    {
    case CIRCLE:
    {
        fs << dynamic_cast<Circle*>(e);
        break;
    }
    case ELLIPSE:
    {
        fs << dynamic_cast<Ellipse*>(e);
        break;
    }
    case RECTANGLE:
    {
        fs << dynamic_cast<Rectangle*>(e);
        break;
    }
    case LINE:
    {
        fs << dynamic_cast<Line*>(e);
        break;
    }
    case GROUP:
    {
        fs << dynamic_cast<Group*>(e);
        break;
    }
    }
    return fs;
}

Element *readElement(ifstream &ifs, int s)
{
    Element *elem;
    switch (s)
    {
    case CIRCLE:
    {
        elem = new Circle();
        ifs >> dynamic_cast<Circle*>(elem);
        break;
    }
    case ELLIPSE:
    {
        elem = new Ellipse();
        ifs >> dynamic_cast<Ellipse*>(elem);
        break;
    }
    case LINE:
    {
        elem = new Line();
        ifs >> dynamic_cast<Line*>(elem);
        break;
    }
    case RECTANGLE:
    {
        elem = new Rectangle();
        ifs >> dynamic_cast<Rectangle*>(elem);
        break;
    }
    case GROUP:
    {
        elem = new Group();
        ifs >> dynamic_cast<Group*>(elem);
        break;
    }
    default:
    {
        exit(1);
    }
    }
    return elem;
}

ofstream & operator <<(std::ofstream &fs, Circle *c)
{
    int name = c->name();
    double x = c->rect().center().x();
    double y = c->rect().center().y();
    double radius = c->rect().width() / 2;
    double rot = c->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&radius, sizeof(radius));
    fs.write((char*)&rot, sizeof(rot));
    return fs;
}

ifstream &operator >>(ifstream &ifs, Circle *c)
{
    double x, y, radius, rot;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&radius, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));
    c->setRect(x - radius, y - radius, 2 * radius, 2 * radius);
    c->setRotation(rot);
    c->norm();
    return ifs;
}

ofstream & operator <<(std::ofstream &fs, Ellipse *e)
{
    int name = e->name();
    double x = e->rect().center().x();
    double y = e->rect().center().y();
    double width = e->rect().width();
    double height = e->rect().height();
    double rot = e->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&width, sizeof(width));
    fs.write((char*)&height, sizeof(height));
    fs.write((char*)&rot, sizeof(rot));
    return fs;
}

ifstream &operator >>(ifstream &ifs, Ellipse *e)
{
    double x, y, width, height, rot;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&width, sizeof(double));
    ifs.read((char*)&height, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));
    e->setRect(x - width / 2, y - height / 2, width, height);
    e->setRotation(rot);
    e->norm();
    return ifs;
}

ofstream & operator <<(std::ofstream &fs, Rectangle *r)
{
    int name = r->name();
    double x = r->rect().center().x();
    double y = r->rect().center().y();
    double width = r->rect().width();
    double height = r->rect().height();
    double rot = r->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&width, sizeof(width));
    fs.write((char*)&height, sizeof(height));
    fs.write((char*)&rot, sizeof(rot));
    return fs;
}

ifstream &operator >>(ifstream &ifs, Rectangle *rec)
{
    double x, y, width, height, rot;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&width, sizeof(double));
    ifs.read((char*)&height, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));
    rec->setRect(x - width / 2, y - height / 2, width, height);
    rec->setRotation(rot);
    rec->norm();
    return ifs;
}

ofstream & operator <<(std::ofstream &fs, Line *l)
{
    int name = l->name();
    QLineF lin = l->line();
    double x1 = lin.x1();
    double y1 = lin.y1();
    double x2 = lin.x2();
    double y2 = lin.y2();

    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x1, sizeof(x1));
    fs.write((char*)&y1, sizeof(y1));
    fs.write((char*)&x2, sizeof(x2));
    fs.write((char*)&y2, sizeof(y2));
    return fs;
}

ifstream &operator >>(ifstream &ifs, Line *l)
{
    double x1, y1, x2, y2;
    ifs.read((char*)&x1, sizeof(double));
    ifs.read((char*)&y1, sizeof(double));
    ifs.read((char*)&x2, sizeof(double));
    ifs.read((char*)&y2, sizeof(double));
    l->setLine(x1, y1, x2, y2);
    l->updateGrips();
    return ifs;
}

ofstream & operator <<(std::ofstream &fs, Group *g)
{
    int name = g->name();
    Point p = g->getGroupPoint();
    double x = p.x();
    double y = p.y();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    Element *elem;
    foreach (QGraphicsItem *i, g->childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            fs << elem;
        }
    }
    int end_group = -1;
    fs.write((char*)&end_group, sizeof(int));
    return fs;
}

ifstream &operator >>(ifstream &ifs, Group *g)
{
    int shape, elem_counter = 0;
    double x, y;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    g->groupPoint.setX(x);
    g->groupPoint.setY(y);
    g->updateGrips();

    Element *elem;
    ifs.read((char*)&shape, sizeof(int));
    while (shape != -1)
    {
        elem = readElement(ifs, shape);
        elem_counter++;
        g->addElement(elem);
        ifs.read((char*)&shape, sizeof(int));
    }
    if (elem_counter < 2)
    {
        exit(1);
    }
    return ifs;
}


// funkcje cuts

std::vector<Point>& cutCC(Circle *c1, Circle *c2)
{
    std::vector<Point> points;
    
    return points;
}
