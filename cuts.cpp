#include "cuts.h"
#include "mymath.h"


const Point* Cuts::getPointNearby(const Point &point)
// wyszukuje i zwraca punkt przeciecia bedacy w poblizu punktu point
{
    std::multimap<Point, ShapesPair>::iterator it;
    for (it = cutsMap.begin(); it != cutsMap.end(); it++)
    {
        if (Math::distance(point, it->first) < OUTLINE_THICKNESS)
        {
            return &it->first;
        }
    }
    return NULL;
}

void Cuts::eraseShape(Shape *s)
// usuwa z mapy przeciec wszystkie przeciecia zwiazane z ksztaltem s
{
    auto it = cutsMap.begin();
    while (it != cutsMap.end())
    {
        if (it->second.hasShape(s))
        {
            cutsMap.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

void Cuts::addCuts(std::vector<Point> points, Shape *s1, Shape *s2)
// dodaje do mapy przeciecia miedzy obiektami s1 oraz s2
{
    ShapesPair sp(s1, s2);
    foreach (Point p, points)
    {
        cutsMap.insert(std::pair<Point, ShapesPair>(p, sp));
    }
}


Cuts::Cuts()
{
}

Cuts::~Cuts()
{
    cutsMap.clear();
}




bool Cuts::ShapesPair::hasShape(Shape *s)
{
    return s1 == s || s2 == s;
}

Cuts::ShapesPair::ShapesPair(Shape *_s1, Shape *_s2): s1(_s1), s2(_s2)
{

}

Cuts::ShapesPair::~ShapesPair()
{

}
