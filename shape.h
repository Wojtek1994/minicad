#ifndef SHAPE_H
#define SHAPE_H

#include <QPainter>
#include <QtGui>
#include "Canvas.h"

class Shape
{

private:
    Canvas* c;


public:
    Shape(Canvas*);
    ~Shape();
};

#endif // SHAPE_H
