#include "scene.h"
#include "shapes.h"
#include "MainWindow.h"

#include <QGraphicsSceneMouseEvent>

#define END_OF_SCENE -2

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (action == BLOCKED)
    {
        return;
    }
    if (action == NONE)
    {
        checkButtons();
    }
    Point point;
    if (specialPoint->isVisible())
    {
        point = specialPoint->getCenter();
        if (specialPoint->getTransforming())
        {
            event->setScenePos(point);
            mouseMoveEvent(event);
        }
    }
    else
    {
        point = event->scenePos();
    }
    switch (action)
    {
    case ROTATING_SET_POINT:
    {
        item->setCentralPoint(point);
        item->setStartRotation();
        item->setRotatable(true);
        setAction(ROTATING);
        break;
    }
    case ROTATING:
    case MOVING:
    case RESIZING:
    {
        finishAction();
        break;
    }
    case DRAWING:
    {
        switch (shape)
        {
        case CIRCLE:
        {
            item = new Circle(point.x(), point.y(), 0);
            break;
        }
        case ELLIPSE:
        {
            item = new Ellipse(point.x(), point.y(), 0, 0);
            break;
        }
        case RECTANGLE:
        {
            item = new Rectangle(point.x(), point.y(), 0, 0);
            break;
        }
        case LINE:
        {
            item = new Line(point.x(), point.y(), point.x(), point.y());
            break;
        }
        }
        addElement(item, shape);
        item->setResizable(true);
        setAction(RESIZING);
        break;
    }
    case CHOOSING_POINT:
    {
        setAction(BLOCKED);
        dynamic_cast<MainWindow*>(parent())->getInterpreter()->gotPoint(point);
        break;
    }
    case NONE:
    {
        if (scenePressed(event))
        {
            pressed = true;
            pressPoint = event->scenePos();
            selectingRect->setRect(0, 0, 0, 0);
            selectingRect->setVisible(true);        // selectingRect przygotowany
        }
        break;
    }
    }
}


template <typename T>
void Scene::selectItems(const T &rec_or_point, bool precise)
// zaznaczanie obiektow prostokatem lub przez klikniecie
{
    QList <Element*> selected_items;
    Element* elem;
    Shape* sh;
    foreach (QGraphicsItem *i, items())
    {
        if ((sh = dynamic_cast<Shape*> (i)) != NULL)
        {
            if (sh->trySelect(rec_or_point, precise))
            {
                elem = dynamic_cast<Element*> (i->topLevelItem());
                if (!selected_items.contains(elem))
                {
                    selected_items.append(elem);
                }
            }
        }
    }
    foreach (Element* e, selected_items)
    {
        e->check(!e->isChecked());
    }
}

void Scene::sceneClicked(const Point &p)
{
    selectItems(p, false);
}

bool Scene::scenePressed(QGraphicsSceneMouseEvent *e)
// zwraca true, jezeli nie zostal nacisniety zaden z grip'ow; w przeciwnym wypadku false i wywoluje metode
// dla nacisnietego grip'a
{
    Grip* grip;
    foreach (QGraphicsItem *i, items())
    {
        if (i->isVisible())
        {
            if ((grip = dynamic_cast<Grip*> (i)) != NULL)
            {
                if (grip->hasPointInside(e->scenePos()))
                {
                    grip->mousePressEvent(e);
                    return false;
                }
            }
        }
    }
    return true;
}

void Scene::deleteItems(QList<Element *> &list)
// usuwa obiekty z listy 'list'
{
    Element* elem;
    foreach (Element* e, list)
    {
        if (e->name() == GROUP)
        {
            foreach (QGraphicsItem *i, dynamic_cast<QGraphicsItem*>(e)->childItems())
            {
                if ((elem = dynamic_cast<Element*>(i)) != NULL)
                {
                    deleteElement(elem);
                }
            }
            deleteElement(e);
        }
        else
        {
            deleteElement(e);
        }
    }
}

void Scene::deleteAllItems()
// usuniecie fizyczne wszystkich elementow
{
    Element* elem;
    foreach (QGraphicsItem *i, items())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            delete elem;
        }
    }
}

void Scene::updateCuts(Element *elem)
{
    deleteFromCuts(elem);
    addToCuts(elem);
}

void Scene::deleteFromCuts(Element *elem)
// usuwa elementy z mapy punktow przeciecia
{
    Element *e;
    if (elem->name() == GROUP)
    {
        foreach (QGraphicsItem *i, dynamic_cast<QGraphicsItem*>(elem)->childItems())
        {
            if ((e = dynamic_cast<Element*>(i)) != NULL)
            {
                deleteFromCuts(e);
            }
        }
    }
    else
    {
        cuts->eraseShape(dynamic_cast<Shape*>(elem));
    }
}

void Scene::deleteFromCuts(QList<Element *> &list)
// usuwa liste elementow z mapy punktow przeciecia
{
    foreach (Element *e, list)
    {
        deleteFromCuts(e);
    }
}

void Scene::addToCuts(Element *elem)
// dodaje element do mapy punktow przeciecia
{
    Element *e;
    if (elem->name() == GROUP)
    {
        foreach (QGraphicsItem *i, dynamic_cast<QGraphicsItem*>(elem)->childItems())
        {
            if ((e = dynamic_cast<Element*>(i)) != NULL)
            {
                addToCuts(e);
            }
        }
    }
    else
    {
        Shape *this_shape = dynamic_cast<Shape*>(elem);
        std::vector<Point> points;
        Shape* sh;
        foreach (QGraphicsItem *i, items())
        {
            if ((sh = dynamic_cast <Shape*>(i)) != NULL && sh != this_shape)
            {
                points = Shape::findCuts(this_shape, sh);
                if (!points.empty())
                {
                    cuts->addCuts(points, this_shape, sh);
                }
            }
        }
    }
}


void Scene::checkSpecialPoint(const Point &p)
// sprawdzenie czy nalezy wyswietlic punkt charakterystyczny
{
    if (!findGripsNearby(p))
    {
        if (!findCutNearby(p))
        {
            specialPoint->setVisible(false);
        }
    }
}

bool Scene::findGripsNearby(const Point &p)
// sprawdzenie czy w poblizu danego punktu znajduja sie jakies grip'y
{
    Grip* grip;
    QGraphicsItem* qitem;
    try
    {
        qitem = dynamic_cast<QGraphicsItem*>(item);
    }
    catch (...)
    {
        qitem = NULL;
    }

    foreach (QGraphicsItem* i, items())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            if (!grip->hasInHierarchy(qitem) || !specialPoint->getTransforming())
            {
                if (grip->hasPointInside(p))
                {
                    specialPoint->setOnPoint(grip->rect().center());
                    specialPoint->setVisible(true);
                    return true;
                }
            }
        }
    }
    return false;
}

bool Scene::findCutNearby(const Point &p)
// sprawdzenie czy w poblizu punktu p sa punkty przeciecia
{
    const Point* point = cuts->getPointNearby(p);
    if (point != NULL)
    {
        specialPoint->setOnPoint(*point);
        specialPoint->setVisible(true);
        return true;
    }
    return false;
}

void Scene::finishAction()
// zakonczenie akcji - obracania, przesuwania lub zmiany rozmiaru
{
    switch (action)
    {
    case ROTATING:
    {
        item->setRotatable(false);
        item->norm();
        updateCuts(item);
        break;
    }
    case MOVING:
    {
        item->setMovable(false);
        item->updateGrips();
        updateCuts(item);
        break;
    }
    case RESIZING:
    {
        item->setResizable(false);
        item->updateGrips();
        updateCuts(item);
        break;
    }
    }
    shape = UNKNOWN;
    setAction(NONE);
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (action == BLOCKED)
    {
        return;
    }
    if (!pressed)
        // pressed == false - np. po skonczeniu rysowania, wybierania punktu
    {
        return;
    }
    pressed = false;
    selectingRect->setVisible(false);
    if (action == NONE && pressPoint.x() == event->scenePos().x() && pressPoint.y() == event->scenePos().y())
    {
        sceneClicked(pressPoint);
        return;
    }
    selectItems(selectingRect->rect(), selectingRect->getType());
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    double x = event->scenePos().x();
    double y = event->scenePos().y();
    QString napis("");
    napis.append(QString::number(x));
    napis.append(", ");
    napis.append(QString::number(y));
    dynamic_cast<MainWindow*>(parent())->getCoordsLabel()->setText(napis);

    checkSpecialPoint(event->scenePos());
    if (specialPoint->isVisible())
    {
        QString napis("");
        napis.append(QString::number(specialPoint->x()));
        napis.append(", ");
        napis.append(QString::number(specialPoint->y()));
        dynamic_cast<MainWindow*>(parent())->getSpecialCoordsLabel()->setText(napis);
    }
    if (action == BLOCKED)
    {
        return;
    }
    if (action != NONE && action != CHOOSING_POINT)
    {
        switch (shape)
        {
        case CIRCLE:
        {
            dynamic_cast<Circle*>(item)->mouseMoveEvent(event);
            break;
        }
        case ELLIPSE:
        {
            dynamic_cast<Ellipse*>(item)->mouseMoveEvent(event);
            break;
        }
        case RECTANGLE:
        {
            dynamic_cast<Rectangle*>(item)->mouseMoveEvent(event);
            break;
        }
        case LINE:
        {
            dynamic_cast<Line*>(item)->mouseMoveEvent(event);
            break;
        }
        case GROUP:
        {
            dynamic_cast<Group*>(item)->mouseMoveEvent(event);
            break;
        }
        }
    }
    else
    {
        if (pressed)
        {
            selectingRect->mouseMoveEvent(pressPoint, event);
        }
    }
}

void Scene::keyPressEvent(QKeyEvent *event)
{
    if (action == BLOCKED || action == CHOOSING_POINT)
    {
        return;
    }
    switch (event->key())
    {
    case Qt::Key_Delete:
    {
        QList<Element*> list = getCheckedElements();
        deleteFromCuts(list);
        deleteItems(list);
        break;
    }
    case Qt::Key_C:
    {
        clipboard->save(getCheckedElements());
        break;
    }
    case Qt::Key_V:
    {
        checkAll(false);
        foreach (Element *e, clipboard->get())
        {
            Element *new_elem = Element::copyElement(e);
            addElement(new_elem, new_elem->name());
            new_elem->updateGrips();
            addToCuts(new_elem);
            new_elem->check(true);
        }
        break;
    }
    case Qt::Key_Q:
    {
        checkAll(false);
        break;
    }
    case Qt::Key_W:
    {
        checkAll(true);
        break;
    }
    }
}


void Scene::addElement(Element *e, SHAPES s)
{
    switch (s)
    {
    case CIRCLE:
    {
        addItem(dynamic_cast<QGraphicsEllipseItem*>(e));
        break;
    }
    case ELLIPSE:
    {
        addItem(dynamic_cast<QGraphicsEllipseItem*>(e));
        break;
    }
    case RECTANGLE:
    {
        addItem(dynamic_cast<QGraphicsRectItem*>(e));
        break;
    }
    case LINE:
    {
        addItem(dynamic_cast<QGraphicsLineItem*>(e));
        break;
    }
    case GROUP:
    {
        addItem(dynamic_cast<QGraphicsItemGroup*>(e));
        break;
    }
    }
}

void Scene::deleteElement(Element *elem)
// usuniecie obiektu (fizycznie)
{
    removeItem(dynamic_cast<QGraphicsItem*>(elem));
    delete elem;
}

void Scene::removeElement(Element *elem)
// usuniecie obiektu ze sceny
{
    removeItem(dynamic_cast<QGraphicsItem*>(elem));
}

void Scene::createGroup(const QList<Element *> &checked_elements, const Point &point)
// stworzenie grupy z listy elementow z punktem charakterystycznym - point
{
    Group *gr = new Group(point);
    addElement(gr, gr->name());
    for (int i = checked_elements.length() - 1; i >= 0; i--)
    {
        removeElement(checked_elements.at(i));
        gr->addElement(checked_elements.at(i));
    }
}

void Scene::destroyGroup(const QList<Element*> &checked_elements)
// rozbicie grupy i dodanie wszystkich elementow do sceny pojedynczo
{
    Group* group;
    Element *elem, *e;
    for (int i = checked_elements.length() - 1; i >= 0; i--)
    {
        e = checked_elements.at(i);
        if ((group = dynamic_cast<Group*>(e)) != NULL)
        {
            removeElement(e);
            foreach (QGraphicsItem* i, group->childItems())
            {
                if ((elem = dynamic_cast<Element*>(i)) != NULL)
                {
                    addElement(elem, elem->name());
                }
            }
            delete e;
        }
    }
}

void Scene::setItemShape(SHAPES s)
{
    shape = s;
}

void Scene::setItem(Element *e)
{
    item = e;
}

void Scene::setAction(ACTIONS a)
// ustawienie aktualnej akcji sceny (rysowanie, obracanie, itp.)
{
    if (a == ROTATING || a == MOVING || a == RESIZING)
    {
        specialPoint->setTransforming(true);
    }
    else
    {
        if (a == BLOCKED || a == CHOOSING_POINT)
        {
            finishAction();
        }
        specialPoint->setTransforming(false);
    }
    action = a;
}

void Scene::checkAll(bool b)
{
    Element* elem;
    foreach (QGraphicsItem *g, items())
    {
        if ((elem = dynamic_cast<Element*>(g)) != NULL)
        {
             elem->check(b);
        }
    }
}

QList <Element*> Scene::getCheckedElements()
{
    QList <Element*> list;
    Element* elem;
    foreach (QGraphicsItem* i, items())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            if (elem->isChecked())
            {
                list.append(elem);
            }
        }
    }
    return list;
}

void Scene::checkButtons()
// sprawdzenie przyciskow odpowiedzialnych za rysowanie
{
    if (dynamic_cast<MainWindow*>(parent())->getShapeButtons()->areChecked())
    {
        setAction(DRAWING);
        shape = dynamic_cast<MainWindow*>(parent())->getShapeButtons()->getShape();
    }
}


Scene::Scene(QWidget *parent): shape(UNKNOWN), item(NULL), action(NONE), pressed(false)
{
    setParent(parent);
    selectingRect = new SelectingRect(0, 0, 0, 0);
    specialPoint = new SpecialPoint();
    cuts = new Cuts();
    clipboard = new Clipboard();
    addItem(selectingRect);
    addItem(specialPoint);
}

Scene::~Scene()
{
    delete selectingRect;
    delete specialPoint;
    deleteAllItems();
    delete cuts;
    delete clipboard;
}


QRectF Scene::SelectingRect::calcResizing(const Point &mouse, const Point &base)
// obliczanie rozmiarow selectingRect'a na podstawie myszy oraz nieruchomego wierzcholka base
{
    double width = mouse.x() - base.x();
    double height = mouse.y() - base.y();
    double corner_x, corner_y;
    if (width < 0)
    {
        width *= -1;
        corner_x = mouse.x();
        setBrush(Qt::green);
    }
    else
    {
        corner_x = base.x();
        setBrush(Qt::blue);
    }
    if (height < 0)
    {
        height *= -1;
        corner_y = mouse.y();
    }
    else
    {
        corner_y = base.y();
    }
    return QRectF(corner_x, corner_y, width, height);
}

void Scene::SelectingRect::mouseMoveEvent(const Point &origin, QGraphicsSceneMouseEvent *event)
{
    setRect(calcResizing(event->scenePos(), origin));
}

bool Scene::SelectingRect::getType()
// type true - zaznaczanie oknem; type false - zaznaczenie przez przeciecie
{
    if (brush() == Qt::blue)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Scene::SelectingRect::SelectingRect(const double &_x, const double &_y, const double &_w, const double &_h)
    :QGraphicsRectItem(_x, _y, _w, _h)
{
    setVisible(false);
    setBrush(Qt::blue);
    setOpacity(0.5);            // polprzezroczysty
    setZValue(30);              // ustawienie zawsze ponad wszystkimi innymi obiektami
}

Scene::SelectingRect::~SelectingRect()
{

}


void Scene::SpecialPoint::setOnPoint(const Point &p)
{
    setX(p.x() - SPECIAL_POINT_SIZE / 2);
    setY(p.y() - SPECIAL_POINT_SIZE / 2);
}

void Scene::SpecialPoint::setTransforming(bool b)
{
    transforming = b;
}

bool Scene::SpecialPoint::getTransforming()
{
    return transforming;
}

Point Scene::SpecialPoint::getCenter()
{
    return Point(x() + SPECIAL_POINT_SIZE / 2, y() + SPECIAL_POINT_SIZE / 2);
}

Scene::SpecialPoint::SpecialPoint(): transforming(false)
{
    setZValue(20);      // ustawienie punktu zawsze ponad reszta obiektow (oprocz selectingRect (z = 30))
    QPolygon p;
    p.append(QPoint(0, 1));
    p.append(QPoint(1, 0));
    p.append(QPoint(5, 4));
    p.append(QPoint(9, 0));
    p.append(QPoint(10, 1));
    p.append(QPoint(6, 5));
    p.append(QPoint(10, 9));
    p.append(QPoint(9, 10));
    p.append(QPoint(5, 6));
    p.append(QPoint(1, 10));
    p.append(QPoint(0, 9));
    p.append(QPoint(4, 5));                 // tworzenie wielokata wygladajacego jak X - punkt charakterystyczny
    QPen pen(QColor(0, 0, 255));            // niebieski
    setPen(pen);
    setBrush(Qt::blue);
    setPolygon(p);
    setVisible(false);
}

Scene::SpecialPoint::~SpecialPoint()
{

}


void Scene::Clipboard::save(QList<Element *> &list)
{
    clipboard.clear();
    foreach (Element *e, list)
    {
        clipboard.push_back(Element::copyElement(e));
    }
}

QList<Element *> &Scene::Clipboard::get()
{
    foreach (Element *e, clipboard)
    {
        Point move = dynamic_cast<QGraphicsItem*>(e)->mapFromScene(*MOVE_VEC);
        e->move(move.x(), move.y());             // wklejane elementy beda przesuniete o wektor [30, 30]
    }
    return clipboard;
}

Scene::Clipboard::Clipboard()
{
    MOVE_VEC = new Point(30, 30);
}

Scene::Clipboard::~Clipboard()
{
    delete MOVE_VEC;
    clipboard.clear();
}


std::ofstream& operator <<(std::ofstream &fs, Scene *s)
{
    int end_scene = END_OF_SCENE;
    Element* elem;
    QList <QGraphicsItem*> item_list = s->items();
    for (int i = item_list.length() - 1; i >= 0; i--)
    {
        elem = dynamic_cast<Element*>(item_list.at(i));
        if (elem != NULL)
        {
            if (item_list.at(i)->parentItem() == NULL)
            {
                fs << elem;
            }
        }
    }
    fs.write((char*)&end_scene, sizeof(int));
    return fs;
}

std::ifstream& operator >>(std::ifstream &ifs, Scene *sc)
{
    int shape;
    Element *elem;
    ifs.read((char*)&shape, sizeof(int));
    while (shape != END_OF_SCENE)
    {
        elem = readElement(ifs, shape);
        sc->addElement(elem, elem->name());
        sc->updateCuts(elem);
        ifs.read((char*)&shape, sizeof(int));
    }

    return ifs;
}

