#ifndef MATH_H
#define MATH_H

#include "point.h"
#include "shapes.h"
#include <vector>

class Math
{
private:
    static std::vector<double> getEllipseCoeffs(const Point &center, double major, double minor, double rot);
    static std::vector<double> quadraticEquation(double a, double b, double c);
    static const Point *linearEquationsSystem(double a1, double b1, double c1, double a2, double b2, double c2);
    static int sgn(double d);
    static double cbrt(double d);
    static double safeAcos(const double &d);
    static std::vector<double> squareEquation(double a, double b, double c, double d);
    static std::vector<double> ferrari(double a, double b, double c, double d, double e);
    static const Point *cutLL_v2(Point p11, Point p12, Point p21, Point p22);
    static std::vector<Point> cutEE_v2(Point p1, double major1, double minor1, double rot1, Point p2, double major2, double minor2, double rot2);
    static std::vector<Point> cutEL_v2(Point center, double w, double h, Point p1, Point p2);

public:

    static std::vector<Point> cutCC(Circle *, Circle *);
    static std::vector<Point> cutCE(Circle *, Ellipse *);
    static std::vector<Point> cutCR(Circle *, Rectangle *);
    static std::vector<Point> cutCL(Circle *, Line *);
    static std::vector<Point> cutEE(Ellipse *, Ellipse *);
    static std::vector<Point> cutER(Ellipse *, Rectangle *);
    static std::vector<Point> cutEL(Ellipse *, Line *);
    static std::vector<Point> cutRR(Rectangle *, Rectangle *);
    static std::vector<Point> cutRL(Rectangle *, Line *);
    static std::vector<Point> cutLL(Line *, Line *);
    static std::vector<Point> cutER_v2(QRectF e_rec, Point p1, Point p2, Point p3, Point p4);
    static std::vector<Point> cutRL_v2(Point p1, Point p2, Point p3, Point p4, Point p11, Point p12);
    static std::vector<Point> cutRR_v2(Point p1, Point p2, Point p3, Point p4, Point p5, Point p6, Point p7, Point p8);

    static bool pointsInsideRect(const QRectF &rec, const std::vector<Point> vp);
    static double calculateAngle(const Point &, const Point &);
    static Point calculateRotatedPoint(Point, Point, const double &);
    static double distance(const Point &p1, const Point &p2);
    static double closestX(const QRectF&, const Point &);
    static double closestY(const QRectF&, const Point &);


    Math();
    ~Math();
};

#endif // MATH_H
