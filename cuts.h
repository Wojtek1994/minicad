#ifndef CUTS_H
#define CUTS_H

#include <map>
#include "point.h"
#include "shapes.h"

class Shape;

class Cuts
{
    class ShapesPair
    {
        Shape* s1;
        Shape* s2;

    public:
        bool hasShape(Shape*);

        ShapesPair(Shape*, Shape*);
        ~ShapesPair();
    };

    std::multimap <Point, ShapesPair> cutsMap;

public:
    const Point* getPointNearby(const Point&);
    void eraseShape(Shape*);
    void addCuts(std::vector<Point>, Shape*, Shape*);

    Cuts();
    ~Cuts();
};

#endif // CUTS_H
