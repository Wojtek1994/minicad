#include "button.h"
#include "scene.h"

ShapeButtons::Button::Button(int type, QWidget* parent): QPushButton(parent)
{
    setVisible(true);
    setCheckable(true);
    setChecked(false);
    setGeometry(0, type * 30, 50, 25);
    switch (type)
    {
    case 0:
    {
        setText("Circle");
        break;
    }
    case 1:
    {
        setText("Ellipse");
        break;
    }
    case 2:
    {
        setText("Rect");
        break;
    }
    case 3:
    {
        setText("Line");
        break;
    }
    }
}

ShapeButtons::Button::~Button()
{

}

void ShapeButtons::Button::mousePressEvent(QMouseEvent *event)
{
    if (isChecked())
    {
        setChecked(false);
    }
    else
    {
        dynamic_cast<ShapeButtons*>(parent())->checkAll(false);
        setChecked(true);
    }
}

bool ShapeButtons::areChecked()
{
    return circle->isChecked() || ellipse->isChecked() || rectangle->isChecked() || line->isChecked();
}

SHAPES ShapeButtons::getShape()
{
    if (circle->isChecked())
    {
        return CIRCLE;
    }
    if (ellipse->isChecked())
    {
        return ELLIPSE;
    }
    if (rectangle->isChecked())
    {
        return RECTANGLE;
    }
    if (line->isChecked())
    {
        return LINE;
    }
    return UNKNOWN;
}

void ShapeButtons::checkAll(bool b)
{
    circle->setChecked(b);
    ellipse->setChecked(b);
    rectangle->setChecked(b);
    line->setChecked(b);
}

ShapeButtons::ShapeButtons(QWidget *parent): QWidget(parent)
{
    setGeometry(1120, 20, 50, 115);
    circle = new Button(0, this);
    ellipse = new Button(1, this);
    rectangle = new Button(2, this);
    line = new Button(3, this);
    setVisible(true);
}

ShapeButtons::~ShapeButtons()
{
    delete circle;
    delete ellipse;
    delete rectangle;
    delete line;
}
