#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <QPlainTextEdit>
#include <QLineEdit>
#include <vector>
#include <algorithm>

class Element;      // forward declaration
class Scene;        // forward declaration
class Point;        // forward declaration

class CommandEdit: public QLineEdit
{
private:
    const int MAX_LINE_LENGTH;
    enum STATE {KEY_WORD_NEEDED, POINTS_DATA_NEEDED, VALUE_NEEDED};
    enum REQUESTED_ACTION {GROUPING, UNGROUPING, MOVING, ROTATING, NONE};
    QPlainTextEdit *buffer;
    QString readRequest(QString &);
    QString gotKeyWord(const QString &);

    QList <Element*> checkedElements;

    std::vector <QString> keyWords;

    STATE state;
    REQUESTED_ACTION reqAction;

    void initKeyWords(void);
    REQUESTED_ACTION findKeyWord(const QString &);

    QString printPoint(const Point &);
    QString readPoint(QString &);
    QString gotValue(QString &);
    void breakAction(void);
    Scene *getScene(void);


protected:
    void keyPressEvent(QKeyEvent *);

public:
    void gotPointsData(Point, bool = true);

    CommandEdit(QPlainTextEdit *, QWidget* = 0);
    ~CommandEdit();
};


class Interpreter: public QWidget
{
private:
    const int MAX_BUFFER_SIZE;
    QPlainTextEdit *textBuffer;
    CommandEdit *commandEdit;

public:
    Scene *getScene(void);
    void gotPoint(Point);
    void setRect(const QRect&);

    Interpreter(QWidget* = 0);
    ~Interpreter();
};


#endif // INTERPRETER_H
