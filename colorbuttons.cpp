#include "colorbuttons.h"
#include <QColorDialog>
#include "MainWindow.h"

void BrushButton::mouseReleaseEvent(QMouseEvent *e)
{
    QColor color = QColorDialog::getColor(Qt::black, NULL, "Select color");
    if (color.isValid())
    {
        QList<Element*> checked_elems = dynamic_cast<MainWindow*>(parent())->getScene()->getCheckedElements();
        foreach (Element *e, checked_elems)
        {
            e->setElementBrush(color);
        }
    }
    QPushButton::mouseReleaseEvent(e);
}

BrushButton::BrushButton(QWidget *parent): QPushButton(parent)
{
    setText("Change brush");
    setVisible(true);
}

BrushButton::~BrushButton()
{

}


void PenColorButton::mouseReleaseEvent(QMouseEvent *e)
{
    QColor color = QColorDialog::getColor(Qt::black, NULL, "Select color");
    if (color.isValid())
    {
        QList<Element*> checked_elems = dynamic_cast<MainWindow*>(parent())->getScene()->getCheckedElements();
        foreach (Element *e, checked_elems)
        {
            e->setPenColor(color);
        }
    }
    QPushButton::mouseReleaseEvent(e);
}

PenColorButton::PenColorButton(QWidget *parent): QPushButton(parent)
{
    setText("Change lines");
    setVisible(true);
}

PenColorButton::~PenColorButton()
{

}


void TransparentBrushButton::mouseReleaseEvent(QMouseEvent *e)
{
    QList<Element*> checked_elems = dynamic_cast<MainWindow*>(parent())->getScene()->getCheckedElements();
    foreach (Element *e, checked_elems)
    {
        e->setElementBrush(Qt::transparent);
    }
    QPushButton::mouseReleaseEvent(e);
}

TransparentBrushButton::TransparentBrushButton(QWidget *parent):QPushButton(parent)
{
    setText("Set transparent");
    setVisible(true);
}

TransparentBrushButton::~TransparentBrushButton()
{

}
