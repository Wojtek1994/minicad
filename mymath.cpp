#include "mymath.h"
#include <cmath>

using namespace std;

double Math::distance(const Point &p1, const Point &p2)
// zwraca odleglosc miedzy dwoma punkami
{
    return sqrt(pow(p1.x() - p2.x(), 2) + pow(p1.y() - p2.y(), 2));
}

bool Math::pointsInsideRect(const QRectF &rec, const std::vector<Point> vp)
// zwraca true, jezeli kazdy z punktow wektora znajduje sie wewnatrz prostokata
// w przeciwnym wypadku - false
{
    double xmin = rec.topLeft().x();
    double xmax = rec.topRight().x();
    double ymin = rec.topLeft().y();
    double ymax = rec.bottomLeft().y();
    foreach (Point p, vp)
    {
        if (p.x() < xmin || p.x() > xmax || p.y() < ymin || p.y() > ymax)
        {
            return false;
        }
    }
    return true;
}

std::vector<double> Math::getEllipseCoeffs(const Point &center, double major, double minor, double rot)
// zwraca wspolczynniki A, B, C, D, E, F rownania elipsy postaci:
// Ax^2 + Bxy + Cy^2 + Dx + Ey + F = 0
{
    std::vector<double> result;
    double cos_rot = cos(rot), sin_rot = sin(rot), x = center.x(), y = center.y();
    result.push_back(pow(major * sin_rot, 2) + pow(minor * cos_rot, 2));
    result.push_back(2 * (minor * minor - major * major) * sin_rot * cos_rot);
    result.push_back(pow(major * cos_rot, 2) + pow(minor * sin_rot, 2));
    result.push_back(-1 * (2 * result[0] * x + result[1] * y));
    result.push_back(-1 * (result[1] * x + 2 * result[2] * y));
    result.push_back(result[0] * x * x + result[1] * x * y + result[2] * y * y - minor * minor * major * major);
    return result;
}

std::vector<double> Math::quadraticEquation(double a, double b, double c)
// rozwiazuje rownanie kwadratowe zwracajac wektor zawierajacy 0, 1 lub 2 liczby typu double
// bedace rozwiazaniami rownania
// w przypadku nieskonczenie wielu rozwiazan rowniez zwraca pusty wektor
// wartosci wspolczynnikow - dowolne
{
    std::vector<double> result;
    if (abs(a) < EPSILON)
    {
        if (abs(b) < EPSILON)
        {
            // nieskonczenie wiele rozwiazan lub brak rozwiazan (c == 0 lub c != 0)
        }
        else
        {
            result.push_back(-c / b);
        }
    }
    else
    {
        double delta = b * b - 4 * a * c;
        if (abs(delta) < EPSILON)
        {
            result.push_back(-b / 2 / a);
        }
        else
        {
            if (delta < 0)
            {
                // brak rozwiazan
            }
            else
            {
                delta = sqrt(delta);
                result.push_back((-b + delta) / 2 / a);
                result.push_back((-b - delta) / 2 / a);
            }
        }
    }
    return result;
}

const Point* Math::linearEquationsSystem(double a1, double b1, double c1, double a2, double b2, double c2)
// rozwiazuje uklad 2 rownan liniowych postaci:
// a1x + b1y + c1 = 0 oraz a2x + b2y + c2 = 0
// rozwianiem jest para liczb x, y zwrocona w postaci wskaznika na obiekt klasy Point
{
    double det = a1 * b2 - a2 * b1;
    double detx = b1 * c2 - b2 * c1;
    double dety = a2 * c1 - a1 * c2;
    if (abs(det) < EPSILON)
    {
        // uklad sprzeczny lub nieskonczenie wiele rozwiazan
        return NULL;
    }
    else
    {
        Point *p = new Point(detx / det, dety / det);
        return p;
    }
}

int Math::sgn(double d)
// znak liczby (-1 dla ujemnych, 1 dla dodatnich lub 0 dla 0)
{
    if (d < 0)
    {
        return -1;
    }
    else
    {
        if (d > 0)
        {
            return 1;
        }
        return 0;
    }
}

double Math::cbrt(double d)
// zwraca pierwiastek trzeciego stopnia z dowolnej liczby rzeczywistej
{
    return sgn(d) * pow(abs(d), 1.0/3);
}

double Math::safeAcos(const double &d)
// zwraca arccos z liczby rzeczywistej z przedzialu <-1; 1>, poza tym przedzialem wyniki to -PI lub 1
{
    if (d >= 1)
    {
        return 0;
    }
    else
    {
        if (d <= -1)
        {
            return -PI;
        }
        else
        {
            return acos(d);
        }
    }
}

std::vector<double> Math::squareEquation(double a, double b, double c, double d)
// zwraca rozwiazanie rownania 3-ciego stopnia postaci:
// ax^3 + bx^2 + cx + d = 0
// wektor rozwiazan moze zawierac 0, 1, 2 lub 3 liczby - w zaleznosci od liczby rozwiazan
// wartosci wspolczynnikow - dowolne
{
    std::vector<double> result;
    if (abs(a) < EPSILON)
    // rownanie kwadratowe
    {
        return quadraticEquation(b, c, d);
    }
    double p = c / a - b * b / 3 / a / a;
    double q = 2 * b * b * b / 27 / a / a / a + d / a - b * c / 3 / a / a;
    double delta = pow(p / 3, 3) + pow(q / 2, 2);
    double diff = -b / 3 / a;
    if (abs(delta) < EPSILON)
    {
        if (abs(q) < EPSILON)
        {
            result.push_back(cbrt(q / 2) + diff);
        }
        else
        {
            result.push_back(cbrt(q / 2));
            result.push_back(-2 * result[0] + diff);
            result[0] += diff;
        }
    }
    else
    {
        if (delta > 0)
        {
            delta = sqrt(delta);
            result.push_back(cbrt(-q / 2 - delta) + cbrt(- q / 2 + delta) + diff);
        }
        else
        {
            double phi = safeAcos(- q / 2 / sqrt(-pow(p / 3, 3)));
            double y0 = 2 * sqrt(- p / 3);
            result.push_back(y0 * cos(phi / 3) + diff);
            result.push_back(y0 * cos((phi + 2 * PI) / 3) + diff);
            result.push_back(y0 * cos((phi + 4 * PI) / 3) + diff);
        }
    }
    return result;
}

double Math::calculateAngle(const Point &center, const Point &current)
{
    // zwraca kat miedzy dwoma punktami (punkt center - teoretyczny poczatek ukladu wsp.)
    // zwracany kat jest zgodny z ruchem wskazowek zegara (podobnie jak funkcja setRotation)
    double dist;
    if ((dist = Math::distance(current, center)) == 0)
    {
        return 0;
    }
    double angle = Math::safeAcos((current.x() - center.x()) / dist);
    if (center.y() > current.y())
    {
        angle *= -1;
    }
    return angle;
}

Point Math::calculateRotatedPoint(Point point, Point origin, const double &angle)
// oblicza wspolrzedne punktu point obroconego o kat angle (zgodnie z ruchem wskazowek zegara)
// wzgledem punktu origin
{
    point.setX(point.x() - origin.x());
    point.setY(point.y() - origin.y());
    double x = point.x() * cos(angle) - point.y() * sin(angle);
    double y = point.x() * sin(angle) + point.y() * cos(angle);
    return Point(x + origin.x(), y + origin.y());
}


std::vector<double> Math::ferrari(double a, double b, double c, double d, double e)
// zwraca rozwiazanie rownania czwartego stopnia postaci:
// ax^4 + bx^3 + cx^2 + dx + e = 0
// wektor rozwiazan moze zawierac od 0 do 4 liczb
// wartosci wspolczynnikow - dowolne
// rozwiazanie postaci kanonicznej metoda Ferrariego
{
    std::vector<double> result;
    if (abs(a) < EPSILON)
    {
        return squareEquation(b, c, d, e);
    }
    double p = - 3 * b * b / 8 / a / a + c / a;
    double q = b * b * b / 8 / a / a / a - b * c / 2 / a / a + d / a;
    double r = - 3 * b * b * b * b / 256 / a / a / a / a + c * b * b / 16 / a / a / a - b * d / 4 / a / a + e / a;
    std::vector<double> vldouble = squareEquation(-8, -20 * p, 8 * r - 16 * p * p, q * q - 4 * p * p * p + 4 * p * r);
    double v;
    unsigned int i;
    for (i = 0; i < vldouble.size(); i++)
    {
        if (p + vldouble[i] * 2 >= 0)
        {
            v = vldouble[i];
            break;
        }
    }
    if (i == vldouble.size())
    {
        return result;
    }
    double tmp = p + 2 * v;
    double sq = sqrt(tmp);
    if (tmp < EPSILON)
    {
        sq = 0;
        tmp = 1;
    }
    else
    {
        sq = sqrt(tmp);
    }
    vldouble = quadraticEquation(1, sq, p + v - sq * q / 2 / tmp);
    result.insert(result.begin(), vldouble.begin(), vldouble.end());
    vldouble = quadraticEquation(1, - sq, p + v + sq * q / 2 / tmp);
    for (unsigned int i = 0; i < vldouble.size(); i++)
    {
        bool not_in_result = true;
        for (unsigned int j = 0; j < result.size(); j++)
        {
            if (abs(vldouble[i] - result[j]) < EPSILON)
            {
                not_in_result = false;
                break;
            }
        }
        if (not_in_result)
        {
            result.push_back(vldouble[i]);
        }
    }
    for (unsigned int i = 0; i < result.size(); i++)
    {
        result[i] -= b / 4 / a;
    }
    return result;
}

const Point* Math::cutLL_v2(Point p11, Point p12, Point p21, Point p22)
// wszystkie punkty musza byc w jednym ukladzie wspolrzednych (np. we wsp. sceny)
// zwraca punkt przeciecia miedzy dwoma liniami wyznaczonymi przez punkty (p11 i p12) oraz (p21 i p22)
{
    double x11 = p11.x(), y11 = p11.y(), x12 = p12.x(), y12 = p12.y(), x21 = p21.x(), y21 = p21.y(), x22 = p22.x(), y22 = p22.y();
    double a1 = y12 - y11, a2 = y22 - y21, b1 = x11 - x12, b2 = x21 - x22;
    double c1 = - b1 * y11 - a1 * x11;
    double c2 = - b2 * y21 - a2 * x21;
    const Point *p = linearEquationsSystem(a1, b1, c1, a2, b2, c2);
    if (p != NULL)
    {
        if (p->x() >= min(p11.x(), p12.x()) - EPSILON && p->x() <= max(p11.x(), p12.x()) + EPSILON &&
            p->x() >= min(p21.x(), p22.x()) - EPSILON && p->x() <= max(p21.x(), p22.x()) + EPSILON &&
            p->y() >= min(p11.y(), p12.y()) - EPSILON && p->y() <= max(p11.y(), p12.y()) + EPSILON &&
            p->y() >= min(p21.y(), p22.y()) - EPSILON && p->y() <= max(p21.y(), p22.y()) + EPSILON)
        {
            return p;
        }
        else
        {
            delete p;
        }
    }
    return NULL;
}

std::vector<Point> Math::cutRL_v2(Point p1, Point p2, Point p3, Point p4, Point p11, Point p12)
// wszystkie punkty musza byc w jednym ukladzie wspolrzednych (np. we wsp. sceny)
// zwraca punkt przeciecia miedzy prostokatem (o kolejnych wierzcholkach p1, p2, p3, p4) oraz linia o koncach p11 i p12
{
    std::vector<Point> points;
    const Point *p;
    p = cutLL_v2(p11, p12, p1, p2);
    if (p)
    {
        points.push_back(*p);
    }
    p = cutLL_v2(p11, p12, p2, p3);
    if (p)
    {
        points.push_back(*p);
    }
    p = cutLL_v2(p11, p12, p3, p4);
    if (p)
    {
        points.push_back(*p);
    }
    p = cutLL_v2(p11, p12, p4, p1);
    if (p)
    {
        points.push_back(*p);
    }
    return points;
}

std::vector<Point> Math::cutEE_v2(Point p1, double major1, double minor1, double rot1, Point p2, double major2, double minor2, double rot2)
// punkty p1 oraz p2 (srodki elips) musza byc w jednym ukladzie wspolrzednych
// funkcja zwraca punkty przeciecia dwoch elips obliczone na podstawie analitycznych rownan elips
{
    std::vector<Point> points;
    std::vector<double> ve1 = getEllipseCoeffs(p1, major1, minor1, rot1);
    std::vector<double> ve2 = getEllipseCoeffs(p2, major2, minor2, rot2);
    double tmp = ve2[0] / ve1[0];
    std::vector<double> ve3;
    for (unsigned int i = 0; i < ve1.size(); i++)
    {
        ve3.push_back(ve1[i] * tmp - ve2[i]);
    }
    double a = ve2[0] * ve3[2] * ve3[2] - ve2[1] * ve3[1] * ve3[2] + ve2[2] * ve3[1] * ve3[1];
    double b = 2 * ve2[0] * ve3[2] * ve3[4] - ve2[1] * ve3[3] * ve3[2] - ve2[1] * ve3[1] * ve3[4] + 2 * ve2[2] * ve3[1] * ve3[3] - ve2[3] * ve3[1] * ve3[2] + ve2[4] * ve3[1] * ve3[1];
    double c = ve2[0] * ve3[4] * ve3[4] + 2 * ve2[0] * ve3[2] * ve3[5] - ve2[1] * ve3[3] * ve3[4] - ve2[1] * ve3[1] * ve3[5] + ve2[2] * ve3[3] * ve3[3] - ve2[3] * ve3[3] * ve3[2] - ve2[3] * ve3[1] * ve3[4] + 2 * ve2[4] * ve3[1] * ve3[3] + ve2[5] * ve3[1] * ve3[1];
    double d = 2 * ve2[0] * ve3[4] * ve3[5] - ve2[1] * ve3[3] * ve3[5] - ve2[3] * ve3[3] * ve3[4] - ve2[3] * ve3[1] * ve3[5] + ve2[4] * ve3[3] * ve3[3] + 2 * ve2[5] * ve3[1] * ve3[3];
    double e = ve2[0] * ve3[5] * ve3[5] - ve2[3] * ve3[3] * ve3[5] + ve2[5] * ve3[3] * ve3[3];
    std::vector<double> yresult = ferrari(a, b, c, d, e);
    std::vector<double> xresult1, xresult2;
    double y;
    for (unsigned int i = 0; i < yresult.size(); i++)
    {
        y = yresult[i];
        xresult1 = quadraticEquation(ve1[0], ve1[1] * y + ve1[3], ve1[2] * y * y + ve1[4] * y + ve1[5]);
        xresult2 = quadraticEquation(ve2[0], ve2[1] * y + ve2[3], ve2[2] * y * y + ve2[4] * y + ve2[5]);
        foreach (double x1, xresult1)
        {
            foreach (double x2, xresult2)
            {
                if (abs(x1 - x2) < EPSILON)
                {
                    points.push_back(Point(x1, y));
                    break;
                }
            }
        }
    }
    return points;
}

std::vector<Point> Math::cutEL_v2(Point center, double w, double h, Point p1, Point p2)
// wszystkie parametry musza byc we wspolrzednych elipsy (elipsa nieobrocona!)
// punkty p1, p2 - konce odcinka
// rozwiazanie na podstawie rownan analitycznych elipsy oraz linii; zwracane rozwiazania - punkty przeciecia
{
    std::vector<Point> points;
    const Point* p;
    if (w < EPSILON)
    {
        p = cutLL_v2(p1, p2, Point(center.x(), center.y() - h / 2), Point(center.x(), center.y() + h / 2));
        if (p)
        {
            points.push_back(*p);
        }
        return points;
    }
    else
    {
        if (h < EPSILON)
        {
            p = cutLL_v2(p1, p2, Point(center.x() - w / 2, center.y()), Point(center.x() + w / 2, center.y()));
            if (p)
            {
                points.push_back(*p);
            }
            return points;
        }
    }
    double ax2 = w * w / 4, ay2 = h * h / 4;
    double a1 = ay2;
    double b1 = ax2;
    double c1 = - 2 * a1 * center.x();
    double d1 = - 2 * b1 * center.y();
    double e1 = a1 * center.x() * center.x() + b1 * center.y() * center.y() - ax2 * ay2;
    double a2 = p2.y() - p1.y();
    double b2 = p1.x() - p2.x();
    double c2 = - b2 * p1.y() - a2 * p1.x();
    if (abs(a2) < EPSILON)
    {
        if (abs(b2) < EPSILON)
        {
            if (abs(a1 * p1.x() * p1.x() + b1 * p1.y() * p1.y() + c1 * p1.x() + d1 * p1.y() + e1) < EPSILON)
            {
                points.push_back(p1);
            }
        }
        else
        {
            double y = - c2 / b2;
            std::vector<double> vdouble = quadraticEquation(a1, c1, b1 * y * y + d1 * y + e1);
            for (unsigned int i = 0; i < vdouble.size(); i++)
            {
                points.push_back(Point(vdouble[i], y));
            }
        }
    }
    else
    {
        double a3 = - b2 / a2;
        double b3 = - c2 / a2;
        std::vector<double> vdouble = quadraticEquation(a1 * a3 * a3 + b1, 2 * a1 * a3 * b3 + c1 * a3 + d1, a1 * b3 * b3 + c1 * b3 + e1);
        for (unsigned int i = 0; i < vdouble.size(); i++)
        {
            points.push_back(Point(a3 * vdouble[i] + b3, vdouble[i]));
        }
    }
    double xmin, xmax, ymin, ymax;
    if (p1.x() > p2.x())
    {
        xmin = p2.x();
        xmax = p1.x();
    }
    else
    {
        xmin = p1.x();
        xmax = p2.x();
    }
    if (p1.y() > p2.y())
    {
        ymin = p2.y();
        ymax = p1.y();
    }
    else
    {
        ymin = p1.y();
        ymax = p2.y();
    }
    double x, y;
    auto it = points.begin();
    while (it != points.end())
    {
        x = it->x();
        y = it->y();
        if (x < xmin || x > xmax || y < ymin || y > ymax)
        {
            it = points.erase(it);
        }
        else
        {
            it++;
        }
    }
    return points;
}

double Math::closestX(const QRectF & rec, const Point &p)
// zwraca wartosc wspolrzednej x punktu nalezacego do prostokata, ktory jest najblizej punktu p
{
    if (p.x() < rec.x())
    {
        return rec.x();
    }
    else
    {
        if (p.x() > rec.x() + rec.width())
        {
            return rec.x() + rec.width();
        }
        else
        {
            return p.x();
        }
    }
}

double Math::closestY(const QRectF &rec, const Point &p)
// zwraca wartosc wspolrzednej y punktu nalezacego do prostokata, ktory jest najblizej punktu p
{
    if (p.y() < rec.y())
    {
        return rec.y();
    }
    else
    {
        if (p.y() > rec.y() + rec.height())
        {
            return rec.y() + rec.height();
        }
        else
        {
            return p.y();
        }
    }
}


std::vector<Point> Math::cutER_v2(QRectF e_rec, Point p1, Point p2, Point p3, Point p4)
// wszystkie parametry musza byc we wspolrzednych elipsy (elipsa nieobrocona!)
// e_rec - prostokat, w ktory wpisana jest elipsa
// p1, p2, p3, p4 - kolejne wierzcholki prostokata
{
    Point center = e_rec.center();
    double width = e_rec.width();
    double height = e_rec.height();
    std::vector<Point> points = cutEL_v2(center, width, height, p1, p2);
    std::vector<Point> tmp = cutEL_v2(center, width, height, p2, p3);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutEL_v2(center, width, height, p3, p4);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutEL_v2(center, width, height, p4, p1);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    return points;
}

std::vector<Point> Math::cutRR_v2(Point p1, Point p2, Point p3, Point p4, Point p5, Point p6, Point p7, Point p8)
// wszystkie punkty w jednym ukladzie wspolrzednych
// punkty p1, p2, p3, p4 - kolejne wierzcholki jednego z prostokatow, pozostale - drugiego
{
    std::vector<Point> points;
    points = cutRL_v2(p1, p2, p3, p4, p5, p6);
    std::vector<Point> tmp = cutRL_v2(p1, p2, p3, p4, p6, p7);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutRL_v2(p1, p2, p3, p4, p7, p8);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutRL_v2(p1, p2, p3, p4, p8, p5);
    points.insert(points.begin(), tmp.begin(), tmp.end());
    return points;
}


std::vector<Point> Math::cutCC(Circle *c1, Circle *c2)
{
    std::vector<Point> points;
    Point center1 = c1->mapToScene(c1->rect().center());
    Point center2 = c2->mapToScene(c2->rect().center());
    double dx = center1.x() - center2.x();
    double dy = center1.y() - center2.y();
    double rad1 = c1->rect().width() / 2;
    double rad2 = c2->rect().width() / 2;
    double y0;
    std::vector<double> vdouble;
    if (abs(dx) < EPSILON)
    {
        if (abs(dy) < EPSILON)
        {
            // wspolny srodek okregow - pokrywaja sie lub nie maja punktow wspolnych
            return points;      // pusty wektor
        }
        else
        {
            y0 = (dy * (center1.y() + center2.y()) + rad2 * rad2 - rad1 * rad1) / 2 / dy;
            vdouble = quadraticEquation(1, - 2 * center1.x(), center1.x() * center1.x() + pow(center1.y() - y0, 2) - rad1 * rad1);
            for (unsigned int i = 0; i < vdouble.size(); i++)
            {
                points.push_back(Point(vdouble[i], y0));
            }
        }
    }
    else
    {
        double a = -dy / dx;
        double b0 = (rad2 * rad2 - rad1 * rad1 + dy * (center1.y() + center2.y())) / 2 / dx + (center1.x() + center2.x()) / 2;
        double b = b0 - center1.x();
        vdouble = quadraticEquation(a * a + 1, 2 * a * b - 2 * center1.y(), b * b + center1.y() * center1.y() - rad1 * rad1);
        for (unsigned int i = 0; i < vdouble.size(); i++)
        {
            points.push_back(Point(a * vdouble[i] + b0, vdouble[i]));
        }
    }
    return points;
}

std::vector<Point> Math::cutCE(Circle *c, Ellipse *e)
{
    QRectF rec_c = c->rect(), rec_e = e->rect();
    return cutEE_v2(c->mapToScene(rec_c.center()), rec_c.width() / 2, rec_c.height() / 2, 0, e->mapToScene(rec_e.center()), rec_e.width() / 2, rec_e.height() / 2, e->rotation() * PI / 180);
}

std::vector<Point> Math::cutCR(Circle *c, Rectangle *r)
{
    std::vector<Point> points;
    QRectF c_rec = c->rect();
    QRectF r_rec = r->rect();
    points = cutEL_v2(c_rec.center(), c_rec.width(), c_rec.height(), r->mapToItem(c, r_rec.topLeft()), r->mapToItem(c, r_rec.topRight()));
    std::vector<Point> tmp = cutEL_v2(c_rec.center(), c_rec.width(), c_rec.height(), r->mapToItem(c, r_rec.topRight()), r->mapToItem(c, r_rec.bottomRight()));
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutEL_v2(c_rec.center(), c_rec.width(), c_rec.height(), r->mapToItem(c, r_rec.bottomRight()), r->mapToItem(c, r_rec.bottomLeft()));
    points.insert(points.begin(), tmp.begin(), tmp.end());
    tmp = cutEL_v2(c_rec.center(), c_rec.width(), c_rec.height(), r->mapToItem(c, r_rec.bottomLeft()), r->mapToItem(c, r_rec.topLeft()));
    points.insert(points.begin(), tmp.begin(), tmp.end());
    for (unsigned int i = 0; i < points.size(); i++)
    {
        points[i] = c->mapToScene(points[i]);
    }
    return points;
}

std::vector<Point> Math::cutCL(Circle *c, Line *l)
{
    QRectF rec = c->rect();
    std::vector<Point> result = cutEL_v2(rec.center(), rec.width(), rec.height(), l->mapToItem(c, l->line().p1()), l->mapToItem(c, l->line().p2()));
    for (unsigned int i = 0; i < result.size(); i++)
    {
        result[i] = c->mapToScene(result[i]);
    }
    return result;
}

std::vector<Point> Math::cutEE(Ellipse *e1, Ellipse *e2)
{
    QRectF rec1 = e1->rect(), rec2 = e2->rect();
    double degree_to_rad = PI / 180;
    return cutEE_v2(e1->mapToScene(rec1.center()), rec1.width() / 2, rec1.height() / 2, e1->rotation() * degree_to_rad, e2->mapToScene(rec2.center()), rec2.width() / 2, rec2.height() / 2, e2->rotation() * degree_to_rad);
}

std::vector<Point> Math::cutER(Ellipse *e, Rectangle *r)
{
    QRectF e_rec = e->rect();
    QRectF r_rec = r->rect();
    std::vector<Point> points = cutER_v2(e_rec, r->mapToItem(e, r_rec.topLeft()), r->mapToItem(e, r_rec.topRight()), r->mapToItem(e, r_rec.bottomRight()), r->mapToItem(e, r_rec.bottomLeft()));
    for (unsigned int i = 0; i < points.size(); i++)
    {
        points[i] = e->mapToScene(points[i]);
    }
    return points;
}

std::vector<Point> Math::cutEL(Ellipse *e, Line *l)
{
    QRectF rec = e->rect();
    std::vector<Point> result = cutEL_v2(rec.center(), rec.width(), rec.height(), l->mapToItem(e, l->line().p1()), l->mapToItem(e, l->line().p2()));
    for (unsigned int i = 0; i < result.size(); i++)
    {
        result[i] = e->mapToScene(result[i]);
    }
    return result;
}

std::vector<Point> Math::cutRR(Rectangle *r1, Rectangle *r2)
{
    QRectF rec1 = r1->rect();
    QRectF rec2 = r2->rect();
    Point p1 = r1->mapToScene(rec1.topLeft());
    Point p2 = r1->mapToScene(rec1.topRight());
    Point p3 = r1->mapToScene(rec1.bottomRight());
    Point p4 = r1->mapToScene(rec1.bottomLeft());
    Point p5 = r2->mapToScene(rec2.topLeft());
    Point p6 = r2->mapToScene(rec2.topRight());
    Point p7 = r2->mapToScene(rec2.bottomRight());
    Point p8 = r2->mapToScene(rec2.bottomLeft());
    return cutRR_v2(p1, p2, p3, p4, p5, p6, p7, p8);
}

std::vector<Point> Math::cutRL(Rectangle *r, Line *l)
{
    QRectF rec = r->rect();
    Point p11 = l->mapToScene(l->line().p1());
    Point p12 = l->mapToScene(l->line().p2());
    return cutRL_v2(r->mapToScene(rec.topLeft()), r->mapToScene(rec.topRight()), r->mapToScene(rec.bottomRight()), r->mapToScene(rec.bottomLeft()), p11, p12);
}

std::vector<Point> Math::cutLL(Line *l1, Line *l2)
{
    std::vector<Point> points;
    Point p11 = l1->mapToScene(l1->line().p1());
    Point p12 = l1->mapToScene(l1->line().p2());
    Point p21 = l2->mapToScene(l2->line().p1());
    Point p22 = l2->mapToScene(l2->line().p2());
    const Point *cut_point = cutLL_v2(p11, p12, p21, p22);
    if (cut_point)
    {
        points.push_back(*cut_point);
    }
    return points;
}



Math::Math()
{

}

Math::~Math()
{

}

