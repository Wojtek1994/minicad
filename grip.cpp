#include "grip.h"
#include <QBrush>
#include "scene.h"

bool Grip::hasPointInside(Point p)
// zwraca true, jezeli punkt znajduje sie wewnatrz grip'a
{
    p = mapFromScene(p);
    QRectF rec = rect();
    if (p.x() >= rec.x() && p.x() <= rec.x() + GRIP_SIZE && p.y() >= rec.y() && p.y() <= rec.y() + GRIP_SIZE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Grip::getId()
{
    return id;
}

bool Grip::hasInHierarchy(QGraphicsItem *i)
// zwraca true, jezeli obiekt i jest w drzewie parent-child danego grip'a
{
    return dynamic_cast<Element*>(this->parentItem())->hasInHierarchy(i);
}

Grip::Grip(QGraphicsItem *parent)
{
    setParentItem(parent);
    setVisible(false);
    QBrush gray_brush(Qt::lightGray);
    setBrush(gray_brush);
    setZValue(1);           // grip zawsze ponad kazdym innym obiektem
}

Grip::~Grip()
{

}

void Grip::move(const double &x, const double &y)
// grip jest przesuwany o wektor [x, y]
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, GRIP_SIZE, GRIP_SIZE);
}

BorderGrip::BorderGrip(const int &_id, QGraphicsItem *parent): Grip(parent)
{
    id = _id;
}

BorderGrip::~BorderGrip()
{
}

void BorderGrip::mousePressEvent(QGraphicsSceneMouseEvent *e)
// po kliknieciu - mozliwa zmiana wymiarow rodzica
{
    Scene* sc = static_cast<Scene*>(scene());
    sc->setAction(Scene::RESIZING);
    Element* element = dynamic_cast<Element*>(parentItem());
    sc->setItem(element);
    sc->setItemShape(element->name());
    element->childId(id);
    element->check(false);
    element->setResizable(true);
}



CentralGrip::CentralGrip(const int &_id, QGraphicsItem *parent):Grip(parent)
{
    id = _id;
}

CentralGrip::~CentralGrip()
{
}

void CentralGrip::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    Scene* sc = static_cast<Scene*>(scene());
    Element* element = dynamic_cast<Element*>(parentItem());
    element->check(false);
    sc->setItem(element);

    // przy kliknieciu lewym klawiszem - move; prawym - rotate
    if (e->button() == Qt::MouseButton::LeftButton)
    {
        element->setMovable(true);
        sc->setAction(Scene::MOVING);
        sc->setItemShape(element->name());
    }
    else
    {
        sc->setAction(Scene::ROTATING_SET_POINT);
        sc->setItemShape(element->name());
    }
}

