#ifndef IOBUTTONS_H
#define IOBUTTONS_H

#include <QPushButton>
#include <QWidget>

class SaveButton: public QPushButton
{
protected:
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
public:
    SaveButton(QWidget*);
    ~SaveButton();
};

class OpenButton: public QPushButton
{
protected:
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
public:
    OpenButton(QWidget*);
    ~OpenButton();
};


#endif // IOBUTTONS_H
