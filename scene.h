#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include "shapes.h"
#include "cuts.h"

class Element;      // forward declaration
class Cuts;         // forward declaration

enum SHAPES {UNKNOWN, CIRCLE, ELLIPSE, RECTANGLE, LINE, GROUP};

class Scene: public QGraphicsScene
{
private:

    class Clipboard
    {
    private:
        const Point *MOVE_VEC;
        QList <Element*> clipboard;

    public:
        void save(QList <Element*>&);
        QList <Element*> &get();

        Clipboard();
        ~Clipboard();
    };

    class SelectingRect: public QGraphicsRectItem
    {
    private:
        QRectF calcResizing(const Point &, const Point &);

    public:
        void mouseMoveEvent(const Point &, QGraphicsSceneMouseEvent *event);
        bool getType();

        SelectingRect(const double &, const double &, const double &, const double &);
        ~SelectingRect();
    };

    class SpecialPoint: public QGraphicsPolygonItem
    {
    private:
        bool transforming;
        static const int SPECIAL_POINT_SIZE = 10;

    public:

        void setOnPoint(const Point&);
        void setTransforming(bool);
        bool getTransforming(void);

        Point getCenter(void);

        SpecialPoint();
        ~SpecialPoint();
    };


    bool pressed;
    Point pressPoint;
    void checkButtons(void);
    SelectingRect* selectingRect;
    SpecialPoint* specialPoint;
    Cuts* cuts;
    Clipboard *clipboard;

    template <typename T>
    void selectItems(const T &, bool);
    void sceneClicked(const Point &);
    bool scenePressed(QGraphicsSceneMouseEvent*);
    void deleteItems(QList <Element*>&);
    void checkSpecialPoint(const Point &);
    bool findGripsNearby(const Point &);
    bool findCutNearby(const Point&);
    void finishAction(void);

    void deleteFromCuts(Element*);
    void deleteFromCuts(QList<Element*> &);
    void addToCuts(Element*);



protected:
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    void keyPressEvent(QKeyEvent*);


public:
    enum ACTIONS {NONE, ROTATING_SET_POINT, ROTATING, MOVING, RESIZING, DRAWING, CHOOSING_POINT, BLOCKED};

    Scene(QWidget*);
    ~Scene();

    SHAPES shape;
    Element* item;
    ACTIONS action;

    void setItemShape(SHAPES);
    void setItem(Element*);
    void setAction(ACTIONS);
    void checkAll(bool);

    void addElement(Element*, SHAPES);
    void deleteElement(Element*);
    void removeElement(Element*);
    void createGroup(const QList<Element*> &, const Point &);
    void destroyGroup(const QList<Element*> &);
    void deleteAllItems(void);
    void updateCuts(Element*);
    QList <Element*> getCheckedElements(void);

    friend std::ofstream& operator <<(std::ofstream&, Scene*);
    friend std::ifstream& operator >>(std::ifstream&, Scene*);

};



#endif
