#include "interpreter.h"

#include "MainWindow.h"

Scene *Interpreter::getScene()
{
    return dynamic_cast<MainWindow*>(parent())->getScene();
}

void Interpreter::gotPoint(Point point)
{
    commandEdit->gotPointsData(point);
}

void Interpreter::setRect(const QRect &rec)
{
    setGeometry(rec);
    int width = rec.width();
    textBuffer->setGeometry(0, 0, width, MainWindow::TEXTBUFFER_HEIGHT);
    commandEdit->setGeometry(0, textBuffer->geometry().height() + MainWindow::VERTICAL_GAP, width, MainWindow::EDIT_HEIGHT);
}

Interpreter::Interpreter(QWidget *parent): MAX_BUFFER_SIZE(15)
{
    setParent(parent);
    setVisible(true);

    textBuffer = new QPlainTextEdit(this);
    textBuffer->setVisible(true);
    textBuffer->setEnabled(false);
    textBuffer->setMaximumBlockCount(MAX_BUFFER_SIZE);

    commandEdit = new CommandEdit(textBuffer, this);
}

Interpreter::~Interpreter()
{
    delete commandEdit;
    delete textBuffer;
}



QString CommandEdit::gotKeyWord(const QString &key_word)
// wykonuje akcje na zadane polecenie (grouping, ungrouping, itp.)
{
    checkedElements = getScene()->getCheckedElements();
    switch (reqAction = findKeyWord(key_word))
    {
    case GROUPING:
    {
        if (checkedElements.length() < 2)
        {
            breakAction();
            return "Not enough elements selected";
        }
        getScene()->checkAll(false);
        state = POINTS_DATA_NEEDED;
        getScene()->setAction(Scene::CHOOSING_POINT);
        return "Choose central point of the group";
    }
    case UNGROUPING:
    {
        if (checkedElements.length() < 1)
        {
            breakAction();
            return "Not enough elements selected";
        }
        getScene()->checkAll(false);
        getScene()->destroyGroup(checkedElements);
        breakAction();
        return "Groups destroyed!";
    }
    case MOVING:
    {
        if (checkedElements.length() < 1)
        {
            breakAction();
            return "Not enough elements selected";
        }
        getScene()->checkAll(false);
        getScene()->setAction(Scene::BLOCKED);
        state = POINTS_DATA_NEEDED;
        return "Provide vector dx dy";
    }
    case ROTATING:
    {
        if (checkedElements.length() < 1)
        {
            breakAction();
            return "Not enough elements selected";
        }
        getScene()->checkAll(false);
        state = POINTS_DATA_NEEDED;
        getScene()->setAction(Scene::CHOOSING_POINT);
        return "Choose central point of rotation";
    }
    case NONE:
    {
        return "Unknown command";
    }
    }
}

void CommandEdit::initKeyWords()
{
    keyWords.push_back("GROUP");
    keyWords.push_back("UNGROUP");
    keyWords.push_back("MOVE");
    keyWords.push_back("ROTATE");
}

CommandEdit::REQUESTED_ACTION CommandEdit::findKeyWord(const QString &keyWord)
// znajduje lancuch keyWord w kontenerze slow kluczowych
{
    int count = 0;
    foreach (QString str, keyWords)
    {
        if (str.compare(keyWord, Qt::CaseInsensitive) == 0)
        {
            return REQUESTED_ACTION(count);
        }
        count++;
    }
    return REQUESTED_ACTION(count);
}

QString CommandEdit::printPoint(const Point &point)
{
    QString s("(" + QString::number(point.x()) + ", " + QString::number(point.y()) + ")");
    return s;
}

QString CommandEdit::readPoint(QString &str)
{
    str = str.replace(',', '.');
    Point point;
    int space_index;
    if ((space_index = str.indexOf(" ")) != -1)
    {
        point.setX(str.mid(0, space_index).toFloat());
        point.setY(str.mid(space_index + 1, str.length() - space_index).toFloat());
        switch (reqAction)
        {
        case GROUPING:
        {
            gotPointsData(point, false);
            return "Chosen point: " + printPoint(point) + "\nObjects grouped!";
        }
        case MOVING:
        {
            gotPointsData(point, false);
            return "Objects moved by " + printPoint(point);
        }
        case ROTATING:
        {
            gotPointsData(point, false);
            return "Chosen central point of rotation: " + printPoint(point) + "\nProvide angle in degrees";
        }
        }
    }
    else
    {
        breakAction();
        getScene()->setAction(Scene::NONE);
        return "Bad format";
    }
}

QString CommandEdit::gotValue(QString &s)
{
    double value = s.toFloat();
    switch (reqAction)
    {
    case ROTATING:
    {
        foreach (Element* e, checkedElements)
        {
            e->rotate(-value * PI / 180);
            e->norm();
            getScene()->updateCuts(e);
        }
        getScene()->setAction(Scene::NONE);
        breakAction();
        return "Objects rotated by angle: " + QString::number(value);
    }
    }
}

void CommandEdit::breakAction()
{
    reqAction = NONE;
    state = KEY_WORD_NEEDED;
}

Scene *CommandEdit::getScene()
{
    return dynamic_cast<Interpreter*>(parent())->getScene();
}


void CommandEdit::gotPointsData(Point point, bool print)
// metoda wywolywana po nacisnieciu sceny lub wpisaniu wartosci w commandEdit
// wykonuje odpowiednia akcje na podstawie podanego punktu oraz wykonywanej aktualnie operacji (np. grupowanie)
{
    switch (reqAction)
    {
    case GROUPING:
    {
        getScene()->setAction(Scene::NONE);
        getScene()->createGroup(checkedElements, point);
        breakAction();
        if (print)
        {
            buffer->appendPlainText("Chosen point: " + printPoint(point));
            buffer->appendPlainText("Objects grouped!");
        }
        break;
    }
    case MOVING:
    {
        QGraphicsItem* i;
        Point move;
        foreach (Element* e, checkedElements)
        {
            i = dynamic_cast<QGraphicsItem*>(e);
            move = i->mapFromScene(point);
            e->move(move.x(), move.y());
            e->updateGrips();
            getScene()->updateCuts(e);
        }
        if (print)
        {
            buffer->appendPlainText("Objects moved by:" + printPoint(point));
        }
        getScene()->setAction(Scene::NONE);
        breakAction();
        break;
    }
    case ROTATING:
    {
        foreach (Element* e, checkedElements)
        {
            e->setCentralPoint(point);
            e->setStartRotation();
        }
        state = VALUE_NEEDED;
        if (print)
        {
            buffer->appendPlainText("Chosen central point of rotation: " + printPoint(point));
            buffer->appendPlainText("Provide angle in degrees");
        }
        break;
    }
    }
}


QString CommandEdit::readRequest(QString &request)
{
    switch (state)
    {
    case KEY_WORD_NEEDED:
    {
        return gotKeyWord(request);
    }
    case POINTS_DATA_NEEDED:
    {
        return readPoint(request);
    }
    case VALUE_NEEDED:
    {
        return gotValue(request);
    }
    }
}


void CommandEdit::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Enter:
    case Qt::Key_Return:
    {
        QString request(text());
        if (request.compare("") != 0)
        {
            buffer->appendPlainText(request);
        }
        clear();
        buffer->appendPlainText(readRequest(request));
        break;
    }
    default:
    {
        QLineEdit::keyPressEvent(event);
    }
    }
}

CommandEdit::CommandEdit(QPlainTextEdit *pte, QWidget *parent): QLineEdit(parent), buffer(pte), MAX_LINE_LENGTH(30)
{
    setVisible(true);
    QLineEdit::setMaxLength(MAX_LINE_LENGTH);            // maksymalna dlugosc tekstu = 30
    state = KEY_WORD_NEEDED;
    reqAction = NONE;
    initKeyWords();
}


CommandEdit::~CommandEdit()
{
}
