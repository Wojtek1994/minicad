#include "shapes.h"
#include "scene.h"
#include "mymath.h"
#include <cmath>

#include <QMessageBox>

#define END_OF_GROUP -1

using namespace std;



bool Element::isResizable()
{
    return resizable;
}

bool Element::isRotatable()
{
    return rotatable;
}

bool Element::isMovable()
{
    return movable;
}

bool Element::isChecked()
{
    return checked;
}

Element *Element::copyElement(Element *e)
{
    switch (e->name())
    {
    case CIRCLE:
    {
        return new Circle(dynamic_cast<Circle*>(e));
    }
    case ELLIPSE:
    {
        return new Ellipse(dynamic_cast<Ellipse*>(e));
    }
    case RECTANGLE:
    {
        return new Rectangle(dynamic_cast<Rectangle*>(e));
    }
    case LINE:
    {
        return new Line(dynamic_cast<Line*>(e));
    }
    case GROUP:
    {
        return new Group(dynamic_cast<Group*>(e));
    }
    }
}


void Element::childId(const int &_id)
{
    id = _id;
}


bool Element::hasInHierarchy(QGraphicsItem *i)
// zwraca true, jezeli obiekt i jest w hierarchii parent-child tego obiektu
{
    QGraphicsItem* qthis = dynamic_cast<QGraphicsItem*>(this);
    while (qthis)
    {
        if (qthis == i)
        {
            return true;
        }
        else
        {
            qthis = qthis->parentItem();
        }
    }
    return false;
}

Element::Element(): resizable(false), rotatable(false), movable(false), checked(false), id(-1), penColor(Qt::black), linesStyle(false)
{
}

Element::~Element()
{
}




Shape::Shape()
{
}

Shape::~Shape()
{
}

void Shape::setRotatable(bool b)
{
    rotatable = b;
}

std::vector<Point> Shape::findCuts(Shape *s1, Shape *s2)
{
    switch (s1->name())
    {
    case CIRCLE:
    {
        switch (s2->name())
        {
        case CIRCLE:
        {
            return Math::cutCC(dynamic_cast<Circle*>(s1), dynamic_cast<Circle*>(s2));
        }
        case ELLIPSE:
        {
            return Math::cutCE(dynamic_cast<Circle*>(s1), dynamic_cast<Ellipse*>(s2));
        }
        case RECTANGLE:
        {
            return Math::cutCR(dynamic_cast<Circle*>(s1), dynamic_cast<Rectangle*>(s2));
        }
        case LINE:
        {
            return Math::cutCL(dynamic_cast<Circle*>(s1), dynamic_cast<Line*>(s2));
        }
        }
    }
    case ELLIPSE:
    {
        switch (s2->name())
        {
        case CIRCLE:
        {
            return Math::cutCE(dynamic_cast<Circle*>(s2), dynamic_cast<Ellipse*>(s1));
        }
        case ELLIPSE:
        {
            return Math::cutEE(dynamic_cast<Ellipse*>(s1), dynamic_cast<Ellipse*>(s2));
        }
        case RECTANGLE:
        {
            return Math::cutER(dynamic_cast<Ellipse*>(s1), dynamic_cast<Rectangle*>(s2));
        }
        case LINE:
        {
            return Math::cutEL(dynamic_cast<Ellipse*>(s1), dynamic_cast<Line*>(s2));
        }
        }
    }
    case RECTANGLE:
    {
        switch (s2->name())
        {
        case CIRCLE:
        {
            return Math::cutCR(dynamic_cast<Circle*>(s2), dynamic_cast<Rectangle*>(s1));
        }
        case ELLIPSE:
        {
            return Math::cutER(dynamic_cast<Ellipse*>(s2), dynamic_cast<Rectangle*>(s1));
        }
        case RECTANGLE:
        {
            return Math::cutRR(dynamic_cast<Rectangle*>(s1), dynamic_cast<Rectangle*>(s2));
        }
        case LINE:
        {
            return Math::cutRL(dynamic_cast<Rectangle*>(s1), dynamic_cast<Line*>(s2));
        }
        }
    }
    case LINE:
    {
        switch (s2->name())
        {
        case CIRCLE:
        {
            return Math::cutCL(dynamic_cast<Circle*>(s2), dynamic_cast<Line*>(s1));
        }
        case ELLIPSE:
        {
            return Math::cutEL(dynamic_cast<Ellipse*>(s2), dynamic_cast<Line*>(s1));
        }
        case RECTANGLE:
        {
            return Math::cutRL(dynamic_cast<Rectangle*>(s2), dynamic_cast<Line*>(s1));
        }
        case LINE:
        {
            return Math::cutLL(dynamic_cast<Line*>(s1), dynamic_cast<Line*>(s2));
        }
        }
    }
    }
}

void Shape::setMovable(bool b)
{
    movable = b;
}


void Shape::setCentralPoint(const Point &point)
{
    centralPoint = point;
}

void Shape::setStartRotation()
{
    startRotation = dynamic_cast<QGraphicsItem*>(this)->rotation();
}

double Shape::getTotalRotationAngle()
{
    return (dynamic_cast<QGraphicsItem*>(this)->rotation() - startRotation) * PI / 180;
}

void Shape::check(bool b)
{
    checked = b;
    foreach (QGraphicsItem* i, dynamic_cast<QGraphicsItem*>(this)->childItems())
    {
        i->setVisible(b);
    }
    setLinesStyle(b);
}

void Shape::updateGrips()
// ustawienie grip'ow na odpowiednich wspolrzednych po transformacji
{
    Grip* grip;
    QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(this);
    foreach (QGraphicsItem* i, item->childItems())
    {
        grip = dynamic_cast<Grip*>(i);
        grip->setRect(calcPosition(grip->getId()));
    }
}

void Shape::norm()
// obrocenie grip'ow po obroceniu obiektu rodzica - z powrotem - tak, aby obrot byl rowny 0
{
    QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(this);
    foreach (QGraphicsItem *i, item->childItems())
    {
        i->setRotation(-item->rotation());
    }
    updateGrips();
}

void Shape::createGrips(int border_count, int central_count, Shape *parent)
// tworzy grip'y w zaleznosci od przekazanego obiektu parent
{
    for (int i = 0; i < border_count + central_count; i++)
    {
        if (i < border_count)
        {
            new BorderGrip(i, dynamic_cast<QGraphicsItem*>(parent));
        }
        else
        {
            new CentralGrip(i, dynamic_cast<QGraphicsItem*>(parent));
        }
    }
}


Ellipse::Ellipse(const double &_x, const double &_y, const double &_w, const double &_h):QGraphicsEllipseItem(_x, _y, _w, _h)
{
    createGrips(ELLIPSE_BORDER_GRIP_NUM, ELLIPSE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Ellipse::Ellipse(): QGraphicsEllipseItem()
{
    createGrips(ELLIPSE_BORDER_GRIP_NUM, ELLIPSE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Ellipse::Ellipse(Ellipse *e)
{
    penColor = e->penColor;
    setBrush(e->brush());
    createGrips(ELLIPSE_BORDER_GRIP_NUM, ELLIPSE_CENTRAL_GRIP_NUM, this);
    Point p = e->rect().topLeft();
    setRect(p.x(), p.y(), e->rect().width(), e->rect().height());
    setRotation(e->rotation());
    norm();
    setVisible(true);
}

Ellipse::~Ellipse()
{
}

void Ellipse::setResizable(bool b)
{
    resizable = b;
}

void Ellipse::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(QPen(penColor, 1, Qt::DashLine));
    }
    else
    {
        setPen(QPen(penColor, 1, Qt::SolidLine));
    }
}

SHAPES Ellipse::name()
{
    return ELLIPSE;
}

bool Ellipse::trySelect(const Point &p, bool precise)
// zaznaczanie elipsy punktem - jezeli punkt jest w poblizu obrysu - return true
{
    Point point = mapFromScene(p);
    QRectF rec = rect();
    Point center = rec.center();
    double d = sqrt(abs(rec.width() * rec.width() - rec.height() * rec.height())) / 2;
    Point f1, f2;
    double semi_major_axis;
    if (rec.width() > rec.height())
    {
        f1.setY(center.y());
        f2.setY(center.y());
        f1.setX(center.x() - d);
        f2.setX(center.x() + d);
        semi_major_axis = rec.width();
    }
    else
    {
        f1.setY(center.y() - d);
        f2.setY(center.y() + d);
        f1.setX(center.x());
        f2.setX(center.x());
        semi_major_axis = rec.height();
    }
    double dist = abs(Math::distance(point, f1) + Math::distance(point, f2) - semi_major_axis);
    if (precise)
    {
        return dist < OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        return dist < OUTLINE_THICKNESS;
    }
}

bool Ellipse::trySelect(const QRectF &rectangle, bool whole)
// zaznaczanie elipsy prostokatem; whole - cala elipsa musi byc wewnatrz prostokata
{
    QRectF rec = rect();
    Point center = mapToScene(rec.center());
    std::vector<Point> vp;
    double a = rec.width() / 2;
    double b = rec.height() / 2;
    if (a < EPSILON && b < EPSILON)
    {
        vp.push_back(mapToScene(center));
        return Math::pointsInsideRect(rectangle, vp);
    }
    double rot = rotation() * PI / 180;
    double asin_2 = pow(sin(rot) * a, 2);
    double acos_2 = pow(cos(rot) * a, 2);
    double bsin_2 = pow(sin(rot) * b, 2);
    double bcos_2 = pow(cos(rot) * b, 2);
    double xtmp = sqrt(acos_2 + bsin_2);
    double ytmp = sqrt(asin_2 + bcos_2);
    double x1 = center.x() - acos_2 / xtmp - bsin_2 / xtmp;
    double x2 = center.x() + acos_2 / xtmp + bsin_2 / xtmp;
    double y1 = center.y() + asin_2 / ytmp + bcos_2 / ytmp;
    double y2 = center.y() - asin_2 / ytmp - bcos_2 / ytmp;
    vp.push_back(Point(x1, y1));
    vp.push_back(Point(x2, y2));
    bool result = Math::pointsInsideRect(rectangle, vp);
    if (whole)
    {
        return result;
    }
    else
    {
        return result || !(Math::cutER_v2(rec, mapFromScene(rectangle.topLeft()), mapFromScene(rectangle.topRight()), mapFromScene(rectangle.bottomRight()), mapFromScene(rectangle.bottomLeft())).empty());
    }
}
QRectF Ellipse::calcPosition(int i)
// okreslanie pozycji i-tego grip'a
{
    Point point;
    QRectF rec = rect();
    double width = rec.width(), height = rec.height(), x = rec.x(), y = rec.y();
    switch (i)
    {
    case TOP:
    {
        point.setX(x + width / 2);
        point.setY(y);
        break;
    }
    case DOWN:
    {
        point.setX(x + width / 2);
        point.setY(y + height);
        break;
    }
    case LEFT:
    {
        point.setX(x);
        point.setY(y + height / 2);
        break;
    }
    case RIGHT:
    {
        point.setX(x + width);
        point.setY(y + height / 2);
        break;
    }
    case CENTER:
    {
        point.setX(x + width / 2);
        point.setY(y + height / 2);
        break;
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}

void Ellipse::setElementBrush(const QColor &color)
{
    setBrush(color);
}

void Ellipse::setPenColor(const QColor &color)
{
    penColor = color;
    setLinesStyle(linesStyle);
}


void Ellipse::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        QRectF rec = rect();
        Point center = rec.center();

        switch (id)
        {
        case TOP:
        case DOWN:
        {
            double new_dimension = Math::distance(mouse, center);
            setRect(rec.x(), center.y() - new_dimension, rec.width(), 2 * new_dimension);
            break;
        }
        case LEFT:
        case RIGHT:
        {
            double new_dimension = Math::distance(mouse, center);
            setRect(center.x() - new_dimension, rec.y(), 2 * new_dimension, rec.height());
            break;
        }
        default:
        {
            // uzywane przy tworzeniu nowego obiektu
            double half_width = abs(mouse.x() - center.x());
            double half_height = abs(mouse.y() - center.y());
            setRect(center.x() - half_width, center.y() - half_height, 2 * half_width, 2 * half_height);
        }
        }
    }
    if (isMovable())
    {
        Point center = rect().center();
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = Math::calculateAngle(centralPoint, event->scenePos());
        rotate(angle);
    }
}

void Ellipse::move(const double &x, const double &y)
// przesuniecie o wektor [x, y] we wspolrzednych elipsy
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Ellipse::rotate(const double &angle)
// obrot elipsy o kat angle
{
    QRectF rec = rect();
    Point corner = Math::calculateRotatedPoint(mapToScene(rec.topLeft()), centralPoint, angle + (startRotation - rotation()) * PI / 180);
    setRotation(startRotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}


Rectangle::Rectangle(const double &_x, const double &_y, const double &_w, const double &_h)
    : QGraphicsRectItem(_x, _y, _w, _h)
{
    createGrips(RECTANGLE_BORDER_GRIP_NUM, RECTANGLE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Rectangle::Rectangle(): QGraphicsRectItem()
{
    createGrips(RECTANGLE_BORDER_GRIP_NUM, RECTANGLE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Rectangle::Rectangle(Rectangle *r)
{
    penColor = r->penColor;
    setBrush(r->brush());
    createGrips(RECTANGLE_BORDER_GRIP_NUM, RECTANGLE_CENTRAL_GRIP_NUM, this);
    Point p = r->rect().topLeft();
    setRect(p.x(), p.y(), r->rect().width(), r->rect().height());
    setRotation(r->rotation());
    norm();
    setVisible(true);
}

Rectangle::~Rectangle()
{
}

void Rectangle::setResizable(bool b)
// zapamietanie w zmiennej centralPoint nieruchomego wierzcholka podczas zmiany rozmiarow
{
    resizable = b;
    QRectF rec = rect();
    switch (id)
    {
    case TOPL:
    {
        centralPoint.setX(rec.x() + rec.width());
        centralPoint.setY(rec.y() + rec.height());
        break;
    }
    case DOWNL:
    {
        centralPoint.setX(rec.x() + rec.width());
        centralPoint.setY(rec.y());
        break;
    }
    case TOPR:
    {
        centralPoint.setX(rec.x());
        centralPoint.setY(rec.y() + rec.height());
        break;
    }
    case DOWNR:
    {
        centralPoint.setX(rec.x());
        centralPoint.setY(rec.y());
        break;
    }
    default:
    {
        // tylko przy tworzeniu nowego obiektu
        centralPoint.setX(rec.x());
        centralPoint.setY(rec.y());
    }
    }
}

void Rectangle::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(QPen(penColor, 1, Qt::DashLine));
    }
    else
    {
        setPen(QPen(penColor, 1, Qt::SolidLine));
    }
}


SHAPES Rectangle::name()
{
    return RECTANGLE;
}

bool Rectangle::trySelect(const Point &point, bool precise)
// zaznaczanie poprzez klikniecie; zwraca true, jezeli punkt point jest w poblizu obrysu
{
    double outline;
    if (precise)
    {
        outline = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        outline = OUTLINE_THICKNESS;
    }
    Point p = mapFromScene(point);
    QRectF rec = rect();
    if ((abs(rec.x() - p.x()) < outline) ||
        (abs(rec.x() + rec.width() - p.x()) < outline))
    {
        return rec.y() - outline < p.y() && p.y() < rec.y() + rec.height() + outline;
    }
    if ((abs(rec.y() - p.y()) < outline) ||
        (abs(rec.y() + rec.height() - p.y()) < outline))
    {
        return rec.x() - outline < p.x() && p.x() < rec.x() + rec.width() + outline;
    }
    return false;
}

bool Rectangle::trySelect(const QRectF& rec, bool whole)
// zaznaczanie prostokata prostokatem; whole - caly prostokat musi byc wewnatrz
{
    QRectF this_rec = rect();
    Point p1 = mapToScene(this_rec.topLeft());
    Point p2 = mapToScene(this_rec.topRight());
    Point p3 = mapToScene(this_rec.bottomRight());
    Point p4 = mapToScene(this_rec.bottomLeft());
    std::vector<Point> vp;
    vp.push_back(p1);
    vp.push_back(p2);
    vp.push_back(p3);
    vp.push_back(p4);
    bool result = Math::pointsInsideRect(rec, vp);
    if (whole)
    {
        return result;
    }
    else
    {
        return result || !Math::cutRR_v2(p1, p2, p3, p4, rec.topLeft(), rec.topRight(), rec.bottomRight(), rec.bottomLeft()).empty();
    }
}


QRectF Rectangle::calcPosition(int i)
// obliczanie pozycji grip'a
{
    Point point;
    QRectF rec = rect();
    double width = rec.width(), height = rec.height(), x = rec.x(), y = rec.y();
    switch (i)
    {
    case TOPL:
    {
        point.setX(x);
        point.setY(y);
        break;
    }
    case DOWNL:
    {
        point.setX(x);
        point.setY(y + height);
        break;
    }
    case TOPR:
    {
        point.setX(x + width);
        point.setY(y);
        break;
    }
    case DOWNR:
    {
        point.setX(x + width);
        point.setY(y + height);
        break;
    }
    case CENTER:
    {
        point.setX(x + width / 2);
        point.setY(y + height / 2);
        break;
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}


QRectF Rectangle::calcResizing(Point mouse, Point base)
// obliczanie wspolrzednych prostokata na podstawie pozycji myszy oraz nieruchomego wierzcholka
{
    double width = mouse.x() - base.x();
    double height = mouse.y() - base.y();
    double corner_x, corner_y;
    if (width < 0)
    {
        width *= -1;
        corner_x = mouse.x();
    }
    else
    {
        corner_x = base.x();
    }
    if (height < 0)
    {
        height *= -1;
        corner_y = mouse.y();
    }
    else
    {
        corner_y = base.y();
    }
    return QRectF(corner_x, corner_y, width, height);
}

void Rectangle::setElementBrush(const QColor &color)
{
    setBrush(color);
}

void Rectangle::setPenColor(const QColor &color)
{
    penColor = color;
    setLinesStyle(linesStyle);
}

void Rectangle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    Point center = rect().center();
    if (isResizable())
    {
        setRect(calcResizing(mouse, centralPoint));
    }
    if (isMovable())
    {
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = Math::calculateAngle(centralPoint, event->scenePos());
        rotate(angle);
    }
}

void Rectangle::move(const double &x, const double &y)
// przesuwanie prostokata o wektor [x, y] (w jego wspolrzednych)
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Rectangle::rotate(const double &angle)
// metoda obracajaca prostokat o kat angle
{
    QRectF rec = rect();
    Point corner = Math::calculateRotatedPoint(mapToScene(rec.topLeft()), centralPoint, angle + (startRotation - rotation()) * PI / 180);
    setRotation(startRotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}


Line::Line(const double &_x1, const double &_y1, const double &_x2, const double &_y2)
    : QGraphicsLineItem(_x1, _y1, _x2, _y2)
{
    createGrips(LINE_BORDER_GRIP_NUM, LINE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Line::Line(): QGraphicsLineItem()
{
    createGrips(LINE_BORDER_GRIP_NUM, LINE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Line::Line(Line *l)
{
    penColor = l->penColor;
    createGrips(LINE_BORDER_GRIP_NUM, LINE_CENTRAL_GRIP_NUM, this);
    Point p1 = l->mapToScene(l->line().p1());
    Point p2 = l->mapToScene(l->line().p2());
    setLine(p1.x(), p1.y(), p2.x(), p2.y());
    updateGrips();
    setVisible(true);
}

Line::~Line()
{
}

void Line::setResizable(bool b)
{
    resizable = b;
}

void Line::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(QPen(penColor, 1, Qt::DashLine));
    }
    else
    {
        setPen(QPen(penColor, 1, Qt::SolidLine));
    }
}


SHAPES Line::name()
{
    return LINE;
}

bool Line::trySelect(const Point &point, bool precise)
// zwraca true, jezeli punkt znajduje sie w poblizu odcinka
// parametr precise okresla dokladnosc sprawdzenia (OUTLINE_THICKNESS lub OUTLINE_THICKNESS_PRECISE)
{
    double outline;
    if (precise)
    {
        outline = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        outline = OUTLINE_THICKNESS;
    }
    Point p2, p = mapFromScene(point);
    QLineF l = line();
    double x1 = l.x1(), x2 = l.x2(), y1 = l.y1(), y2 = l.y2();
    double dx = x2 - x1;
    double dy = y2 - y1;
    Point pmin, pmax;
    x1 < x2 ? (pmin = Point(x1, y1), pmax = Point(x2, y2)) : (pmin = Point(x2, y2), pmax = Point(x1, y1));

    if (abs(dx) < EPSILON && abs(dy) < EPSILON)
    {
        return Math::distance(p, pmin) < outline;
    }
    else
    {
        double u = ((p.x() - x1) * dx + (p.y() - y1) * dy) / (dx * dx + dy * dy);
        p2.setX(x1 + u * dx);
        p2.setY(y1 + u * dy);
    }

    if (abs(pmin.x() - pmax.x()) < EPSILON)
    {
        if (pmin.y() > pmax.y())
        {
            Point tmp = pmax;
            pmax = pmin;
            pmin = tmp;
        }
        if (p.y() > pmax.y())
        {
            return Math::distance(p, pmax) < outline;
        }
        else
        {
            if (p.y() < pmin.y())
            {
                return Math::distance(p, pmin) < outline;
            }
            else
            {
                return Math::distance(p, p2) < outline;
            }
        }
    }

    if (p2.x() < pmin.x())
    {
        return Math::distance(p, pmin) < outline;
    }
    else
    {
        if (p2.x() > pmax.x())
        {
            return Math::distance(p, pmax) < outline;
        }
        else
        {
            return Math::distance(p, p2) < outline;
        }
    }
}

bool Line::trySelect(const QRectF &rec, bool whole)
// zaznaczenie prostokatem - metoda zwraca true, jezeli linia ma byc zaznaczona
// parametr whole - jezeli true, to cala linia musi byc wewnatrz prostokata
{
    Point p11 = mapToScene(line().p1());
    Point p12 = mapToScene(line().p2());
    std::vector<Point> vp;
    vp.push_back(p11);
    vp.push_back(p12);
    bool result = Math::pointsInsideRect(rec, vp);
    if (whole)
    {
        return result;
    }
    else
    {
        return result || !Math::cutRL_v2(rec.topLeft(), rec.topRight(), rec.bottomRight(), rec.bottomLeft(), p11, p12).empty();
    }
    return false;
}

QRectF Line::calcPosition(int i)
// obliczanie pozycji grip'a w zaleznosci od id
{
    QRectF rec;
    QLineF lin = line();
    Point point;
    switch (i)
    {
    case BEGIN:
    {
        point = lin.p1();
        break;
    }
    case END:
    {
        point = lin.p2();
        break;
    }
    case CENTER:
    {
        point.setX((lin.x1() + lin.x2()) / 2);
        point.setY((lin.y1() + lin.y2()) / 2);
        break;
    }
    }
    point = mapToItem(childItems().at(i), point);
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}

void Line::setElementBrush(const QColor &color)
{
    // niedostepne dla elementu Line
}

void Line::setPenColor(const QColor &color)
{
    penColor = color;
    setLinesStyle(linesStyle);
}


void Line::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QLineF lin = line();
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        switch (id)
        {
        case BEGIN:
        {
            setLine(mouse.x(), mouse.y(), lin.x2(), lin.y2());
            break;
        }
        case END:
        {
            setLine(lin.x1(), lin.y1(), mouse.x(), mouse.y());
            break;
        }
        default:
        {
            // uzywane przy tworzeniu nowego obiektu
            setLine(lin.x1(), lin.y1(), mouse.x(), mouse.y());
        }
        }
    }
    if (isMovable())
    {
        double dx = mouse.x() - (lin.x1() + lin.x2()) / 2;
        double dy = mouse.y() - (lin.y1() + lin.y2()) / 2;
        move(dx, dy);
    }
    if (isRotatable())
    {
        double angle = Math::calculateAngle(centralPoint, event->scenePos());
        rotate(angle);
    }
}

void Line::move(const double &x, const double &y)
// przesuniecie o wektor [x, y] we wspolrzednych linii
{
    QLineF l = line();
    setLine(l.x1() + x, l.y1() + y, l.x2() + x, l.y2() + y);
}

void Line::rotate(const double &angle)
// obrot o kat angle
{
    QLineF l = line();
    Point begin(l.x1(), l.y1());
    begin = Math::calculateRotatedPoint(mapToScene(begin), centralPoint, angle + (startRotation - rotation()) * PI / 180);
    setRotation(startRotation + angle * 180 / PI);
    begin = mapFromScene(begin);
    setLine(begin.x(), begin.y(), begin.x() + l.dx(), begin.y() + l.dy());
}


Circle::Circle(const double &_x, const double &_y, const double &_r)
    : QGraphicsEllipseItem(_x, _y, _r, _r)
{
    createGrips(CIRCLE_BORDER_GRIP_NUM, CIRCLE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Circle::Circle(): QGraphicsEllipseItem()
{
    createGrips(CIRCLE_BORDER_GRIP_NUM, CIRCLE_CENTRAL_GRIP_NUM, this);
    updateGrips();
    setVisible(true);
}

Circle::Circle(Circle *c): QGraphicsEllipseItem()
{
    penColor = c->penColor;
    setBrush(c->brush());
    createGrips(CIRCLE_BORDER_GRIP_NUM, CIRCLE_CENTRAL_GRIP_NUM, this);
    Point p = c->rect().topLeft();
    setRect(p.x(), p.y(), c->rect().width(), c->rect().height());
    setRotation(c->rotation());
    norm();
    setVisible(true);
}

Circle::~Circle()
{
}

void Circle::setResizable(bool b)
{
    resizable = b;
}

void Circle::setLinesStyle(bool b)
{
    linesStyle = b;
    if (b)
    {
        setPen(QPen(penColor, 1, Qt::DashLine));
    }
    else
    {
        setPen(QPen(penColor, 1, Qt::SolidLine));
    }
}


SHAPES Circle::name()
{
    return CIRCLE;
}

bool Circle::trySelect(const Point &p, bool precise)
// zaznaczanie przez klikniecie - zwracana jest wartosc true, jezeli punkt jest w poblizu okregu
{
    Point point = mapFromScene(p);
    QRectF rec = rect();
    Point center = rec.center();
    double radius = rec.width() / 2;
    double precision;
    if (precise)
    {
        precision = OUTLINE_THICKNESS_PRECISE;
    }
    else
    {
        precision = OUTLINE_THICKNESS;
    }
    if (abs(Math::distance(point, center) - radius) < precision)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Circle::trySelect(const QRectF &rec, bool whole)
// zaznaczanie prostokatem; jezeli whole = true, to zostanie zwrocone true, gdy cale kolo jest wewnatrz prostokata
{
    Point circle_center = mapToScene(rect().center());
    double radius = rect().width() / 2;
    double x = Math::closestX(rec, circle_center);
    double y = Math::closestY(rec, circle_center);

    if (whole)
    {
        return rec.contains(QRectF(circle_center.x() - radius, circle_center.y() - radius, 2 * radius, 2 * radius));
    }
    else
    {
        if (Math::distance(circle_center, Point(x, y)) > radius)
        {
            return false;
        }
        else
        {
            return (Math::distance(circle_center, rec.topRight()) > radius) ||
                   (Math::distance(circle_center, rec.topLeft()) > radius) ||
                   (Math::distance(circle_center, rec.bottomRight()) > radius) ||
                   (Math::distance(circle_center, rec.bottomLeft()) > radius);
        }
    }
}


QRectF Circle::calcPosition(int i)
// obliczanie pozycji i-tego grip'a
{
    QRectF rec = rect();
    double width = rec.width(), height = rec.height();
    Point point = mapToScene(rec.center());
    switch (i)
    {
    case TOP:
    {
        point.setX(point.x());
        point.setY(point.y() - height / 2);
        break;
    }
    case DOWN:
    {
        point.setX(point.x());
        point.setY(point.y() + height / 2);
        break;
    }
    case LEFT:
    {
        point.setX(point.x() - width / 2);
        point.setY(point.y());
        break;
    }
    case RIGHT:
    {
        point.setX(point.x() + width / 2);
        point.setY(point.y());
        break;
    }
    case CENTER:
    {
        point.setX(point.x());
        point.setY(point.y());
        break;
    }
    }
    point.setX(point.x() - Grip::GRIP_SIZE / 2);
    point.setY(point.y() - Grip::GRIP_SIZE / 2);
    rec.setRect(point.x(), point.y(), Grip::GRIP_SIZE, Grip::GRIP_SIZE);
    return rec;
}

void Circle::setElementBrush(const QColor &color)
{
    setBrush(color);
}

void Circle::setPenColor(const QColor &color)
{
    penColor = color;
    setLinesStyle(linesStyle);
}

void Circle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Point mouse = mapFromScene(event->scenePos());
    if (isResizable())
    {
        QRectF rec = rect();
        Point center = rec.center();
        double new_radius = Math::distance(mouse, center);
        setRect(center.x() - new_radius, center.y() - new_radius, 2 * new_radius, 2 * new_radius);
    }
    if (isMovable())
    {
        Point center = rect().center();
        move(mouse.x() - center.x(), mouse.y() - center.y());
    }
    if (isRotatable())
    {
        double angle = Math::calculateAngle(centralPoint, event->scenePos());
        rotate(angle);
    }
}

void Circle::move(const double &x, const double &y)
// przesuniecie o wektor [x, y] we wspolrzednych kola
{
    QRectF rec = rect();
    setRect(rec.x() + x, rec.y() + y, rec.width(), rec.height());
}

void Circle::rotate(const double &angle)
// obrot o kat angle
{
    QRectF rec = rect();
    Point corner = Math::calculateRotatedPoint(mapToScene(rec.topLeft()), centralPoint, angle + (startRotation - rotation()) * PI / 180);
    setRotation(startRotation + angle * 180 / PI);
    corner = mapFromScene(corner);
    setRect(corner.x(), corner.y(), rec.width(), rec.height());
}


Group::Group(Point point): QGraphicsItemGroup()
{
    new CentralGrip(0, this);
    groupPoint = point;
    updateGrips();
    setVisible(true);
}

Group::Group()
{
    new CentralGrip(0, this);
    setVisible(true);
}

Group::Group(Group *g)
{
    new CentralGrip(0, this);
    groupPoint = g->groupPoint;
    Element *elem;
    Element *new_elem;
    foreach (QGraphicsItem *i, g->childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            new_elem = copyElement(elem);
            addElement(new_elem);
        }
    }
    updateGrips();
    setVisible(true);
}

Group::~Group()
{
    foreach (QGraphicsItem* i, childItems())
    {
        if (dynamic_cast<Grip*>(i) != NULL)
        {
            delete i;
        }
    }
}

void Group::setMovable(bool b)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setMovable(b);
        }
    }
    movable = b;
}

void Group::setRotatable(bool b)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setRotatable(b);
        }
    }
    rotatable = b;
}

void Group::setResizable(bool b)
{
    // nie mozna zmieniac rozmiaru grupy
}

void Group::setElementBrush(const QColor &color)
{
    Element* elem;
    foreach (QGraphicsItem *i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setElementBrush(color);
        }
    }
}

void Group::setPenColor(const QColor &color)
{
    Element* elem;
    foreach (QGraphicsItem *i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setPenColor(color);
        }
    }
}

void Group::setLinesStyle(bool b)
{
    linesStyle = b;
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setLinesStyle(b);
        }
    }
}

void Group::check(bool b)
{
    checked = b;
    Grip* grip;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            grip->setVisible(b);
            break;
        }
    }
    setLinesStyle(b);
}

void Group::norm()
// wywolanie metody norm dla wszystkich obiektow w grupie
{
    Element* elem;
    Shape* sh;
    double angle;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->norm();
        }
        if ((sh = dynamic_cast<Shape*>(i)) != NULL)
        {
            angle = sh->getTotalRotationAngle();
        }
    }
    groupPoint = Math::calculateRotatedPoint(groupPoint, centralPoint, angle);
    updateGrips();
}

SHAPES Group::name()
{
    return GROUP;
}

void Group::setCentralPoint(const Point &point)
{
    Element* elem;
    centralPoint = point;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setCentralPoint(point);
        }
    }
}

void Group::setStartRotation()
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->setStartRotation();
        }
    }
}

void Group::updateGrips()
{
    Grip* grip;
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((grip = dynamic_cast<Grip*>(i)) != NULL)
        {
            grip->setRect(groupPoint.x() - Grip::GRIP_SIZE / 2, groupPoint.y() - Grip::GRIP_SIZE / 2, Grip::GRIP_SIZE, Grip::GRIP_SIZE);
        }
        else
        {
            if ((elem = dynamic_cast<Element*>(i)) != NULL)
            {
                elem->updateGrips();
            }
        }
    }
}


void Group::addElement(Element *e)
{
    addToGroup(dynamic_cast<QGraphicsItem*>(e));
}

Point Group::getGroupPoint()
{
    return groupPoint;
}

void Group::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isRotatable())
    {
        double angle = Math::calculateAngle(centralPoint, event->scenePos());
        rotate(angle);
    }
    if (isMovable())
    {
        move(event->scenePos().x() - groupPoint.x(), event->scenePos().y() - groupPoint.y());
    }
}


void Group::move(const double &x, const double &y)
{
    Element* elem;
    Point child_move;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            child_move = mapToItem(i, Point(x, y));
            elem->move(child_move.x(), child_move.y());
        }
    }
    groupPoint.setX(groupPoint.x() + x);
    groupPoint.setY(groupPoint.y() + y);
}

void Group::rotate(const double &angle)
{
    Element* elem;
    foreach (QGraphicsItem* i, childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            elem->rotate(angle);
        }
    }
}


std::ofstream &operator <<(std::ofstream &fs, Element *e)
{
    switch(e->name())
    {
    case CIRCLE:
    {
        fs << dynamic_cast<Circle*>(e);
        break;
    }
    case ELLIPSE:
    {
        fs << dynamic_cast<Ellipse*>(e);
        break;
    }
    case RECTANGLE:
    {
        fs << dynamic_cast<Rectangle*>(e);
        break;
    }
    case LINE:
    {
        fs << dynamic_cast<Line*>(e);
        break;
    }
    case GROUP:
    {
        fs << dynamic_cast<Group*>(e);
        break;
    }
    }
    return fs;
}

Element *readElement(std::ifstream &ifs, int s)
{
    Element *elem;
    switch (s)
    {
    case CIRCLE:
    {
        elem = new Circle();
        ifs >> dynamic_cast<Circle*>(elem);
        break;
    }
    case ELLIPSE:
    {
        elem = new Ellipse();
        ifs >> dynamic_cast<Ellipse*>(elem);
        break;
    }
    case LINE:
    {
        elem = new Line();
        ifs >> dynamic_cast<Line*>(elem);
        break;
    }
    case RECTANGLE:
    {
        elem = new Rectangle();
        ifs >> dynamic_cast<Rectangle*>(elem);
        break;
    }
    case GROUP:
    {
        elem = new Group();
        ifs >> dynamic_cast<Group*>(elem);
        break;
    }
    default:
    {
        QMessageBox::information(NULL, "ERROR IO-1", "The file is broken.");
        throw 1;
    }
    }
    return elem;
}

std::ofstream & operator <<(std::ofstream &fs, Circle *c)
{
    int name = c->name();
    double x = c->rect().center().x();
    double y = c->rect().center().y();
    double radius = c->rect().width() / 2;
    double rot = c->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&radius, sizeof(radius));
    fs.write((char*)&rot, sizeof(rot));
    bool not_transparent = c->brush().isOpaque();
    fs.write((char*)&not_transparent, sizeof(bool));
    if (c->brush().isOpaque())
    {
        fs.write((char*)&c->brush().color(), sizeof(QColor));
    }
    fs.write((char*)&c->penColor, sizeof(QColor));
    return fs;
}

std::ifstream &operator >>(std::ifstream &ifs, Circle *c)
{
    double x, y, radius, rot;
    QColor color;
    bool not_transparent;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&radius, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));

    ifs.read((char*)&not_transparent, sizeof(bool));
    if (not_transparent)
    {
        ifs.read((char*)&color, sizeof(QColor));
        c->setElementBrush(color);
    }
    else
    {
        c->setElementBrush(Qt::transparent);
    }

    ifs.read((char*)&color, sizeof(QColor));
    c->setPenColor(color);

    c->setRect(x - radius, y - radius, 2 * radius, 2 * radius);
    c->setRotation(rot);
    c->norm();
    return ifs;
}

std::ofstream & operator <<(std::ofstream &fs, Ellipse *e)
{
    int name = e->name();
    double x = e->rect().center().x();
    double y = e->rect().center().y();
    double width = e->rect().width();
    double height = e->rect().height();
    double rot = e->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&width, sizeof(width));
    fs.write((char*)&height, sizeof(height));
    fs.write((char*)&rot, sizeof(rot));
    bool not_transparent = e->brush().isOpaque();
    fs.write((char*)&not_transparent, sizeof(bool));
    if (e->brush().isOpaque())
    {
        fs.write((char*)&e->brush().color(), sizeof(QColor));
    }
    fs.write((char*)&e->penColor, sizeof(QColor));
    return fs;
}

std::ifstream &operator >>(std::ifstream &ifs, Ellipse *e)
{
    double x, y, width, height, rot;
    QColor color;
    bool not_transparent;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&width, sizeof(double));
    ifs.read((char*)&height, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));

    ifs.read((char*)&not_transparent, sizeof(bool));
    if (not_transparent)
    {
        ifs.read((char*)&color, sizeof(QColor));
        e->setElementBrush(color);
    }
    else
    {
        e->setElementBrush(Qt::transparent);
    }

    ifs.read((char*)&color, sizeof(QColor));
    e->setPenColor(color);

    e->setRect(x - width / 2, y - height / 2, width, height);
    e->setRotation(rot);
    e->norm();
    return ifs;
}

std::ofstream & operator <<(std::ofstream &fs, Rectangle *r)
{
    int name = r->name();
    double x = r->rect().center().x();
    double y = r->rect().center().y();
    double width = r->rect().width();
    double height = r->rect().height();
    double rot = r->rotation();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    fs.write((char*)&width, sizeof(width));
    fs.write((char*)&height, sizeof(height));
    fs.write((char*)&rot, sizeof(rot));
    bool not_transparent = r->brush().isOpaque();
    fs.write((char*)&not_transparent, sizeof(bool));
    if (r->brush().isOpaque())
    {
        fs.write((char*)&r->brush().color(), sizeof(QColor));
    }
    fs.write((char*)&r->penColor, sizeof(QColor));
    return fs;
}

std::ifstream &operator >>(std::ifstream &ifs, Rectangle *rec)
{
    double x, y, width, height, rot;
    QColor color;
    bool not_transparent;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    ifs.read((char*)&width, sizeof(double));
    ifs.read((char*)&height, sizeof(double));
    ifs.read((char*)&rot, sizeof(double));

    ifs.read((char*)&not_transparent, sizeof(bool));
    if (not_transparent)
    {
        ifs.read((char*)&color, sizeof(QColor));
        rec->setElementBrush(color);
    }
    else
    {
        rec->setElementBrush(Qt::transparent);
    }

    ifs.read((char*)&color, sizeof(QColor));
    rec->setPenColor(color);

    rec->setRect(x - width / 2, y - height / 2, width, height);
    rec->setRotation(rot);
    rec->norm();
    return ifs;
}

std::ofstream & operator <<(std::ofstream &fs, Line *l)
{
    int name = l->name();
    QLineF lin = l->line();
    double x1 = lin.x1();
    double y1 = lin.y1();
    double x2 = lin.x2();
    double y2 = lin.y2();

    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x1, sizeof(x1));
    fs.write((char*)&y1, sizeof(y1));
    fs.write((char*)&x2, sizeof(x2));
    fs.write((char*)&y2, sizeof(y2));
    fs.write((char*)&l->penColor, sizeof(QColor));
    return fs;
}

std::ifstream &operator >>(std::ifstream &ifs, Line *l)
{
    double x1, y1, x2, y2;
    QColor color;
    ifs.read((char*)&x1, sizeof(double));
    ifs.read((char*)&y1, sizeof(double));
    ifs.read((char*)&x2, sizeof(double));
    ifs.read((char*)&y2, sizeof(double));
    ifs.read((char*)&color, sizeof(QColor));
    l->setLine(x1, y1, x2, y2);
    l->updateGrips();
    l->setPenColor(color);
    return ifs;
}

std::ofstream & operator <<(std::ofstream &fs, Group *g)
{
    int name = g->name();
    Point p = g->getGroupPoint();
    double x = p.x();
    double y = p.y();
    fs.write((char*)&name, sizeof(int));
    fs.write((char*)&x, sizeof(x));
    fs.write((char*)&y, sizeof(y));
    Element *elem;
    foreach (QGraphicsItem *i, g->childItems())
    {
        if ((elem = dynamic_cast<Element*>(i)) != NULL)
        {
            fs << elem;
        }
    }
    int eog = END_OF_GROUP;     // == -1; koniec zapisu grupy
    fs.write((char*)&eog, sizeof(int));
    return fs;
}

std::ifstream &operator >>(std::ifstream &ifs, Group *g)
{
    int shape, elem_counter = 0;
    double x, y;
    ifs.read((char*)&x, sizeof(double));
    ifs.read((char*)&y, sizeof(double));
    g->groupPoint.setX(x);
    g->groupPoint.setY(y);
    g->updateGrips();

    Element *elem;
    ifs.read((char*)&shape, sizeof(int));
    while (shape != END_OF_GROUP)
    {
        elem = readElement(ifs, shape);
        elem_counter++;
        g->addElement(elem);
        ifs.read((char*)&shape, sizeof(int));
    }
    if (elem_counter < 2)
    {
        QMessageBox::information(NULL, "ERROR IO-2", "The file is broken.");
        throw 2;
    }
    return ifs;
}


