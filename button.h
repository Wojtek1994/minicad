#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>
#include <QObject>
#include <QWidget>

enum SHAPES;

class ShapeButtons: public QWidget
{
    class Button: public QPushButton
    {
    protected:
        void mousePressEvent(QMouseEvent*);

    public:
        Button(int type, QWidget *parent);
        ~Button();
    };

    Button *circle;
    Button *ellipse;
    Button *rectangle;
    Button *line;
public:
    bool areChecked(void);
    SHAPES getShape(void);
    void checkAll(bool);

    ShapeButtons(QWidget *parent = 0);
    ~ShapeButtons();
};


#endif // BUTTON_H
