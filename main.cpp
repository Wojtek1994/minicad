#include "MainWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    try
    {
        a.exec();
    }
    catch (int i)
            // wyjatek lapany przy blednym odczycie z pliku (blad nr 1 lub 2)
    {
        return i;
    }

    return 0;
}


